
# Data

The path to the different data files are specified in the function path\_processed\_files of the 'paths.py' script located in main. All csv files must contain as first column the sample identifier in the form: SITECODE\_DATE. In order to run the analysis the following files should be provided:

- *list\_of\_samples*: a json file containing the list of samples to be included in the analysis.
- *clusters*: a csv file that contains clusters. The file must contain a *cluster* column.
- *IMPROVE*: a csv file containing the IMPROVE measurements in micrograms per square centimeters. The file must contain columns *OCf:Value* and *OCf:Unc*.
- *PLS1*: a csv file containing aCH, aCOH, COOH and CO predictions in moles in function of the number of components. The file must contain a *variable* column (containing which FG predictions it is aCH, aCOH, COOH or CO), a *sample* column (the sample identifier in the form: SITECODE\_DATE), a *predicted* column and a *ncomp* column.
- *NH2*: a csv file containing aNH2 predictions in moles. The file must contain a column named *aNH2*.
- *COO*: a csv file containing COO predictions in moles. The file must contain a column named *COO*.

# Input file (model\_spe.json)

To run one analysis a 'run' folder has to be created in results containing a 'model\_spe.json' file. An example is provided in 'results/run01/'.

The file must contain:

- *order\_params*: a dict that will specify the order of parameters in the parameter vector. (In 'model\_spe.json', the specification of "bounds" vector elements should follow this order.)
- *bounds*: bounds for the parameters if a  uniform prior is chosen in order defined by order\_params. If a different type of prior is added the limit has to be set to Infinity, as shown in example file.

The file can contain:

- priors for all the continuous parameters *alpha*, *lambda_aCH* (b1), *lambda_aCOH* (b3) and eventually kappa. The priors can be a Gaussian distribution (type = 'normal') with *mean* and *stdev*, a Weibull distribution (type = 'weibull') with *shape* and *scale*, aand a beta distribution (type ='beta') with 4 parameters *shape1*, *shape2*, *a* and *b* (*a* and *b* correspond to min and max values). 
- *kappa_intercept* if *kappa* is not fixed
- *PLS*: the name of the PLS model, if not indicated it will be "PLS1"
- *path\_cluster* the path to the cluster file, if not indicated will be clusters\_10.csv
- *prior\_ncomp\_aCH*, *aCOH*, *COOH* discrete priors for the number of latent variables

Once this is done, the run\_all.py script can be executed with as an argument the name of the run folder. The name of the run folder is the only non optional argument, there is also the argument 'clusters' which should contain the list of clusters on which the analysis has to be carried on, if not specified the analysis will only be made one cluster 1. The final argument is "what\_to\_run" is a 3-element boolean vector, which indiciates whether to run: 1) Laplace method, 2) Metropolis, and 3) visualisation. If not specified, everything will be computed. A number of different setting can be changed directly in the run\_all.py file. At each run, the settings from the run\_all.py file will be written in the results folder, together with the parameter settings for the analysis.

# Settings

## Laplace

The value for each parameter is given in the order specified by *order\_params* in the model\_spe file.
- *starting\_point*:         starting point of the optimization process
- *bnds\_laplace*:           bounds between which the parameters posterior is estimated
- *steps\_laplace*:          at which resolution for each parameter the posterior is estimated
- *max\_diff\_marginal*:     the log value difference under which a set of number of latent variables is considered negligeable

## Metropolis

- *number\_of\_chain*:   the number of chains to be run
- *iterations*:          number of iterations to be run
- *calibration*:         number of iterations to calibrate the standard deviation for the proposal distribution
- *output\_results*:     at which frequency to output the results
- *number\_of\_runs*:    number of chains to be run
- *initial\_stds*:       inital standard deviation for proposal distrobution
- *burn\_in*:            number of iterations to consider as burn-in and not used to calculated standard deviation
