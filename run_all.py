
# coding: utf-8

# In[35]:


import warnings
import ast
warnings.filterwarnings("ignore", category=RuntimeWarning) 

from pathlib import Path
import sys

path = Path().resolve()
sys.path.append(f'{path.parents[0]}/')
from main.models.laplace_LBFGS_B import laplace_LBFGS_B_run
from main.models.metropolis import metropolis_run


from main.visualizing.visualizing_all import visualising_all




what_to_run_default =[1, 1, 1]

params_LBFGS_B = dict()
params_LBFGS_B['starting_point'] = [0.75, 0.48, 0.4, 0.018]
params_LBFGS_B['bnds_laplace'] = [[0.5, 1], [0, 1], [0, 1.5], [0,1]]
params_LBFGS_B['steps_laplace'] = [0.001, 0.001, 0.005, 0.001]
params_LBFGS_B['max_diff_marginal'] = 20
    
params_metropolis = dict()
params_metropolis['number_of_chain'] = 2
params_metropolis['iterations'] = 10**6
params_metropolis['calibration'] = 3*10**5
params_metropolis['output_results'] = 10**6
params_metropolis['burn_in'] = int(0.7*10**5)
params_metropolis['initial_stds'] = [0.02, 0.02, 0.02, 0.01, 2, 2, 2]


def add_run_type(dict_,run):
    dict_['run'] = run
    return (dict_)

def _run_models(run,
                cluster,
                what_to_run,
                params_LBFGS_B = None,
                params_Nelder_Mead = None,
                params_BFGS = None,
                params_metropolis = None):
    params_metropolis['cluster'] = cluster
    params_LBFGS_B['cluster'] = cluster

    if what_to_run[0]:
        params_LBFGS_B = add_run_type(params_LBFGS_B, run)
        laplace_LBFGS_B_run(**params_LBFGS_B)
        
    if what_to_run[1]:
        params_metropolis = add_run_type(params_metropolis, run)
        metropolis_run(**params_metropolis)
    
    return


def run_model(run,
               clusters = [1],
               what_to_run = what_to_run_default):

    for cluster in clusters:
        print('Analysis started for cluster '+str(cluster))
        _run_models(run,
                   cluster,
                   what_to_run_default,
                   params_LBFGS_B = params_LBFGS_B,
                   params_metropolis = params_metropolis)
        
    visualising_all(run)
    return
    

if __name__ =="__main__":
    run = 'run01'
    run_model(run, clusters = [3])

