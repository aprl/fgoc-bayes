import os
from pathlib import Path
import sys

#Import main path
path = Path(__file__).parents[1]
sys.path.append(path)


root = Path(__file__).parents[1]
results_folder = os.path.join(root,'results')


def create_folder(folder):
    """Create folder if it doesn't exist
    
    :param folder    folder to be created
    :return          -
    """
    if not os.path.exists(folder):
        os.makedirs(folder)
    return

def path_raw_files():
    """Get path of raw files in function of year
                                   
    :return: dict containing path to
       
       spectra: raw spectra
       improve_data: IMPROVE measurements file
      
    """
    FILES = {
        'spectra'     : None, # .rds
        'improve_data': None  # .txt
    }
    return(FILES)


def path_processed_files():
    """Get path of processed files in function of year                                  
    :return: dict containing path to
       
       IMPROVE: csv containing IMPROVE measurements
       spectra: baseline corrected spectra
       NH2: NH2  predictions
       CONO2: CONO2 predictions
       COO: COO predictions
       clusters: cluster affiliation
       PLS1: predictions made with PLS1 of aCH, aCOH, COOH and CO in function of the number of latent variables used
       list_of_samples: list of samples for which we have IMPROVE measurements and specta
       volume: IMPROVE sampled volume
 
    """
    data_folder_processed = os.path.join(data_folder, 'processed')

    FILES_PROCESSED = {
        'list_of_samples' : None, # .json
        'clusters'        : None, # .csv
        'IMPROVE'         : None, # .csv
        'PLS1'            : None, # .csv        
        'NH2'             : None, # .csv
        'COO'             : None, # .csv
        'spectra'         : None, # .csv        
        'volume'          : None  # .csv
    }
    return(FILES_PROCESSED)


def path_model_spe(run):
    """Get path of model_specifications file in function of year
    and type of site
    
    :param run:     name of run
                                   
    :return: path to model specifications json file
       
    """
    path_ = os.path.join(results_folder, run)
    path_ = os.path.join(path_, 'model_spe.json')

    return(path_)
