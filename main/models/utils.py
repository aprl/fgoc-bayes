import numpy as np
import pandas as pd

import scipy.stats as stats
import math

from pathlib import Path
import sys
import json

path = Path().resolve()
sys.path.append(f'{path.parents[2]}/')

from main.paths import  path_model_spe, path_processed_files



molar_masses = dict()
molar_masses['C'] = 12.011
molar_masses['O'] = 15.999
molar_masses['H'] =  1.008
molar_masses['N'] = 14.007

lambdas_other_FGs = dict()
lambdas_other_FGs['aNH2'] = 0.5
lambdas_other_FGs['COO'] = 1
lambdas_other_FGs['CO'] = 1
lambdas_other_FGs['CONO2'] = 0
lambdas_other_FGs['COOH'] = 1

filter_diameter_cm = 2.12


def change_site_code_2011_inverse(index_):
    if len(index_.split('_'))>1:
          if index_.split('_')[1].startswith('2011'):
            sitecode = index_.split('_')[0].replace('1', 'X').replace('PHOEX', 'PHX1A').replace('PHOE5', 'PHX5A')
            index_ = sitecode + '_' +index_.split('_')[1]
    return index_

def check_if_file_exist(path):
    """Check if file exist

    :param path:    path of the file

    :return: boolean
    """

    return Path(path).is_file()


def mmoles_per_cm2(value):
    """ Change units from mu moles to mu moles/cm2
    using IMPROVE teflon filter are

    :param value:   value in mu moles

    return:         value in mu moles/cm2
    """
    c=value*4/(math.pi*filter_diameter_cm**2)
    return c


def read_json(path):
    """Read json

    :param path:    path of the file

    :return: dict
    """
    with open(path, 'r') as fp:
        dict_=json.load(fp)
    return dict_

def _check_content_model_spe(model_spe):
    """Check that all elements that must be in the model_spe dict are there and raise and error if not

    :param model_spe:    dict model_spe

    :return: -
    """
    missing = list(set(['order_params', 'bounds']) - set(model_spe.keys()))
    test = missing 
    if test:
        raise ValueError('Model spe file does not specify:', test)
    return

def get_model_spe(run):
    """Read and check model_spe json

    :param run:    name of run

    :return: dict model_spe
    """
    model_spe_path = path_model_spe(run)
    if check_if_file_exist(model_spe_path)==False:
        print(model_spe_path)
        raise FileNotFoundError('Model_spe file doesnt exist')
    model_spe = read_json(model_spe_path)
    _check_content_model_spe(model_spe)
    return(model_spe)
#Has to be changed to be generalisable for 2013!

def get_data_for_cluster(list_of_samples, clusters, cluster = None):
    """Takes the dataframe and return a dataframe containing data only for specific cluster.
        Raise an error if cluster is not contained in dataframe

    :param list_of_samples:    list_of_samples that are in predictions
    :param clusters:           dataframe containing index of samples and its cluster
    :param cluster:            cluster for which we want the data

    :return: list_of_samples   list of samples that are in that cluster
    """

    if cluster is not None:
        list_of_samples = list_of_samples[list_of_samples.isin(clusters[clusters['cluster']==cluster].index.to_series())]
    if len(list_of_samples)==0:
        raise ValueError(f'Cluster "{cluster}" not found in dataframe.')
    return (list_of_samples)

def get_arrays_ncomp(df):
    """Takes the dataframe and return three arrays containing PLS predictions

    :param df:    full dataframe

    :return: one array of predictions per each functional group
    """
    length_ = len(df[df['ncomp']==1])
    dim = int(length_/len(set(df['variable'])))
    dim2 = len(set(df['ncomp']))

    aCH_ = np.empty(shape=(1, dim2, dim))
    aCOH_= np.empty(shape=(1, dim2, dim))
    COOH_ = np.empty(shape=(1, dim2, dim))

    for x in range(dim2):
        frame = df[df['ncomp']==x+1]
        aCH_[0, x] = frame[frame['variable']=='aCH']['predicted'].values
        aCOH_[0, x] = frame[frame['variable']=='aCOH']['predicted'].values
        COOH_[0, x] = frame[frame['variable']=='COOH']['predicted'].values
    return(aCH_, aCOH_, COOH_)


def _check_other_FGS_data(other_FGs, temp):
    """ Check that all samples in prediction models also appear in fixed FGs prediction
    and raise error if it is not the case

    :param other_FGs: dataframe containing fixed FGs prediction
    :param temp: dataframe containing variable FGs prediciton in function of number of latent variables

    :return: -
    """
    if len(temp) != len(other_FGs):
        raise ValueError('All the samples are not in the other FGs dataset')
    return

def get_data(PLS, path_cluster, cluster =None, return_df =False):
    """From PLS name and path to cluster file , read the according data, keep only the interesting values 
    OC values and FGs predictions (in function of number of latent variable) and fixed FGs

    :param PLS:             name of PLS model
    :param path_cluster:    path to cluster file
    :param cluster:         cluster of interest
    :param return_df:       if dataframe containing fixed FGs predictions and IMPROVE OC values must be returned

    :return: OC values, OC uncertainties, predictions aCH , aCOH and COOH in function of ncomp
             and predictions of aNH2, COO, CO and CONO2
    """
    paths_data = path_processed_files()
    path_data = paths_data['IMPROVE']
    if not check_if_file_exist(path_data):
        print(path_data)
        raise FileNotFoundError('Data file does not exist')
    df = pd.read_csv(path_data, index_col=0)

    df.index = df.index.to_series().apply(lambda x: change_site_code_2011_inverse(x))
    cols_to_keep_IMPROVE = ['OCf:Value', 'OCf:Unc']
    df = df[cols_to_keep_IMPROVE]

    NH2 = pd.read_csv(paths_data['NH2'], index_col = 0)
    df = df.join(NH2, how = 'inner')

    # CONO2 = pd.read_csv(paths_data['CONO2'], index_col = 0)
    # df = df.join(CONO2, how = 'inner')
    df['CONO2'] = 0

    COO = pd.read_csv(paths_data['COO'], index_col = 0)
    df = df.join(COO, how = 'inner')

    list_of_samples = pd.read_json(paths_data['list_of_samples'], typ = 'series').apply(lambda x: change_site_code_2011_inverse(x))

    clusters = pd.read_csv(path_cluster, index_col =0)



    list_of_samples = get_data_for_cluster(list_of_samples, clusters, cluster)


    if (~list_of_samples.isin(df.index.to_series())).any():
        raise ValueError('FGs file or IMPROVE file does not contain all the samples defined in list of samples file')
    else:
        df = df[df.index.to_series().isin(list_of_samples)]

    if df.isna().any().any():
        raise ValueError('FGs file or IMPROVE file contains for samples definied in list of samples')
    PLS_model = pd.read_csv(paths_data[PLS], index_col = 1)

    if (~list_of_samples.isin(PLS_model.index.to_series())).any():

        raise ValueError('Predictions file does not contain all the samples defined in list of samples file')
    else:
        PLS_model = PLS_model[PLS_model.index.to_series().isin(list_of_samples)]

    ## Change units of PLS predictions from mu moles to mu moles/cm2
    PLS_model['predicted'] = PLS_model['predicted'].apply(lambda x: mmoles_per_cm2(x))
    #PLS_model.to_csv('test.csv')


    all_together = PLS_model.join(df, how = 'inner')
    if len(PLS_model)!=len(all_together):
        print('problem!!')

    # renaming Carboxylate to COO
    all_together = all_together.rename(columns={'carboxylate': 'COO'})

    aCH_, aCOH_, COOH_ = get_arrays_ncomp(all_together)
    df = all_together[(all_together['ncomp']==10) & ( all_together['variable']=='CO') ]

    CO = df['predicted'].values
    Y = df['OCf:Value'].values
    unc = df['OCf:Unc'].values
    aNH2 = df['aNH2'].values
    COO = df['COO'].values
    CONO2 = df['CONO2'].values

    if return_df:
        return (Y, unc, aCH_, aCOH_, COOH_, aNH2, COO, CO, CONO2, df.drop('predicted', 1))
    else:
        return(Y, unc, aCH_, aCOH_, COOH_, aNH2, COO, CO, CONO2)


def _create_functions(aCH_,
                      aCOH_ ,
                      COOH_,
                      aNH2,
                      COO,
                      CO,
                      CONO2,
                      Y,
                      unc,
                      order_params,
                      bounds,
                      PLS = 'PLS1',
                      path_cluster = None,
                      prior_b1=None,
                      prior_b3=None,
                      prior_kappa=None,
                      kappa_intercept=None,
                      prior_alpha=None,
                      prior_ncomp_aCH=None,
                      prior_ncomp_aCOH=None,
                      prior_ncomp_COOH=None):
    """Takes all useful variable that are going to be fixed for each cluster and create useful
    useful functions that will now only be dependent on the parameters

    :param aCH_:                array of aCH predictions for all ncomp
    :param aCOH_:               array of aCOH predictions for all ncomp
    :param COOH_:               array of COOH predictions for all ncomp
    :param aNH2:                aNH2 predictions
    :param COO:                 COO predictions
    :param CO:                  CO predictions
    :param CONO2:               CONO2 predictions
    :param Y:                   TOR OC measurements
    :param unc:                 TOR OC uncertainties
    :param order_params:        dict containing the defined order of parameters in vector given to each function
    :param bounds:              bounds for uniform priors
    :param PLS:                 name of PLS model
    :param path_cluster:        path to cluster file
    :param prior_b1:            dict containg parameters for normal or Weibull prior for b1
                                if not specified uniform prior with bounds will be used
    :param prior_b3:            dict containg parameters for normal or Weibull prior for b3
                                if not specified uniform prior with bounds will be used
    :param prior_kappa:         dict containing shape and scale for gamma prior for kappa
    :param kappa_intercept:     intercept for regression between uncertainties and OC values
    :param prior_alpha:         dict containg parameters for normal or Weibull prior for alpha
                                if not specified uniform prior with bounds will be used
    :prior ncomp_aCH:           discrete priors for number of latent variables used to predict aCH
    :prior ncomp_aCOH:          discrete priors for number of latent variables used to predict aCOH
    :prior ncomp_COOH:          discrete priors for number of latent variables used to predict COOH

    :return: functions: prior, get_sigmas, log_likelihood, prior_uniform, proposal
    """
    
        

    def _prior_kappa(kappa):
        """Compute prior for kappa

        :param kappa:    kappa

        :return: log probability
        """
        p = stats.gamma.logpdf(kappa, a=prior_kappa['shape'], scale=prior_kappa['scale'])
        return(p)
    
    def _prior_alpha(alpha):
        """Compute prior for alpha

        :param alpha:    alpha

        :return: log probability
        """
        if prior_alpha['type']=='normal':
            p = stats.norm.logpdf(alpha,loc=prior_alpha['mean'], scale=prior_alpha['stdev'])
        elif prior_alpha['type']=='weibull':
            p = stats.weibull_min.logpdf(alpha,c=prior_alpha['shape'],scale=prior_alpha['scale'])
        elif prior_alpha['type']=='beta':
            alpha_prime = (alpha - prior_alpha['a'])/(prior_alpha['b']-prior_alpha['a'])
            p = stats.beta.logpdf(alpha_prime, a = prior_alpha['shape1'], b = prior_alpha['shape2'] )- np.log(prior_alpha['b']-prior_alpha['a'])
        return(p)


    def _prior_b1(b1):
        """Compute prior for b1

        :param b1:    b1

        :return: log probability
        """
        if prior_b1['type']=='normal':
            p = stats.norm.logpdf(b1,loc=prior_b1['mean'], scale=prior_b1['stdev'])
        elif prior_b1['type']=='weibull':
            p = stats.weibull_min.logpdf(b1,c=prior_b1['shape'],scale=prior_b1['scale'])
        elif prior_b1['type']=='beta':
            b1_prime = (b1 - prior_b1['a'])/(prior_b1['b']-prior_b1['a'])
            p = stats.beta.logpdf(b1_prime, a = prior_b1['shape1'], b = prior_b1['shape2'] )- np.log(prior_b1['b']-prior_b1['a'])

        return(p)


    def _prior_b3(b3):
        """Compute prior for b3

        :param b3:    b3

        :return: log probability
        """
        if prior_b3['type']=='normal':
            p = stats.norm.logpdf(b3,loc=prior_b3['mean'], scale=prior_b3['stdev'])
        elif prior_b3['type']=='weibull':
            p = stats.weibull_min.logpdf(b3, c=prior_b3['shape'], scale=prior_b3['scale'])
        elif prior_b3['type']=='beta':
            b3_prime = (b3 - prior_b3['a'])/(prior_b3['b']-prior_b3['a'])
            p = stats.beta.logpdf(b3_prime, a = prior_b3['shape1'], b = prior_b3['shape2'] )- np.log(prior_b3['b']-prior_b3['a'])

        return(p)
    
    def _prior_ncomp_aCH(ncomp_aCH):
        """Compute prior for ncomp_aCH

        :param ncomp_aCH:    ncomp_aCH

        :return: log probability
        """
        p = prior_ncomp_aCH[int(ncomp_aCH)]
        return(p)
    
    
    def _prior_ncomp_aCOH(ncomp_aCOH):
        """Compute prior for ncomp_aCOH

        :param ncomp_aCH:    ncomp_aCOH

        :return: log probability
        """
        p = prior_ncomp_aCOH[int(ncomp_aCOH)]
        return(p)
    
    def _prior_ncomp_COOH(ncomp_COOH):
        """Compute prior for ncomp_aCOH

        :param ncomp_aCH:    ncomp_aCOH

        :return: log probability
        """
        p = prior_ncomp_COOH[int(ncomp_COOH)]
        return(p)

    def prior(params):
        """Compute prior for kappa and if it exist for b1 and b3

        :param params:    vector of params in order specified by params_order

        :return: log probability
        """
        p = 0
        if prior_kappa is not None:
            p = p+ _prior_kappa(params[order_params['kappa']])
        if prior_b1 is not None:
            p = p + _prior_b1(params[order_params['b1']])
        if prior_b3 is not None:
            p = p + _prior_b3(params[order_params['b3']])
        if prior_alpha is not None:
            p = p + _prior_alpha(params[order_params['alpha']])
        if prior_ncomp_aCH is not None:
            p = p + _prior_ncomp_aCH(params[order_params['ncomp_aCH']])
        if prior_ncomp_aCOH is not None:
            p = p + _prior_ncomp_aCOH(params[order_params['ncomp_aCOH']])
        if prior_ncomp_COOH is not None:
            p = p + _prior_ncomp_COOH(params[order_params['ncomp_COOH']])
        
        p = p + prior_uniform(params)
        
        return(p)

        
       

    def get_sigmas(kappa, Y):
        """Get sigmas in function of kappa and OC

        :param kappa:    kappa
        :param Y:        OC values

        :return: array of uncertainties
        """
        sigmas = (kappa_intercept+kappa*Y**2)
        return sigmas
    
    def log_likelihood(parameters):
        """Compute log likelihood

        :param params:    vector of params in order specified by params_order

        :return: log probability
        """

        aCH = aCH_[0, int(parameters[order_params['ncomp_aCH']])]
        aCOH = aCOH_[0, int(parameters[order_params['ncomp_aCOH']])]
        COOH = COOH_[0, int(parameters[order_params['ncomp_COOH']])]
        OC_pred = molar_masses['C'] * (parameters[order_params['b1']] * aCH 
        + parameters[order_params['b3']] * aCOH 
        + lambdas_other_FGs['COOH'] *COOH + lambdas_other_FGs['aNH2'] * aNH2 
        + lambdas_other_FGs['COO'] *COO 
        + lambdas_other_FGs['CO'] *CO 
        + lambdas_other_FGs['CONO2']*CONO2) / parameters[order_params['alpha']]

        if prior_kappa is not None:
            sigmas = get_sigmas(parameters[order_params['kappa']], Y)
        else:
            sigmas = unc
        if any(sigmas<0):
            L = -np.inf
        else:
            t=-(Y-OC_pred)**2/(2*sigmas)-0.5*np.log(sigmas)
            L=np.sum(t)
        return(L)

    def prior_uniform(parameters):
        """Compute uniform prior

        :param params:    vector of params in order specified by params_order

        :return: 0 if params within bounds, -infinity if not
        """
        p = 0
        for elem in order_params:
            if ((parameters[order_params[elem]] < bounds[order_params[elem]][0])|(parameters[order_params[elem]] > bounds[order_params[elem]][1])):
                p = -np.inf
        return(p)


    def _assymetric_proposal_prob_discrete(stds,
                                           normal,
                                           old_elem,
                                           new_elem,
                                           value,
                                           p):
        """Compute log probability of moving one parameter from new to old point divided by
        probability of moving from old to new for discrete parameters

        :param stds:            standard deviations for each truncated normal distribution given
                                in order of params_order
        :param normal:          truncated normal distribution generated for that element
        :param old_elem:        reference value for that parameter
        :param new_elem:        new value for that parameter
        :param value:           index of parameters from order_params
        :param p:               probability already computed for other parameters
        :return:                log probability
        """
        invers_distri=stats.truncnorm((bounds[value][0] - new_elem) / stds[value], (bounds[value][1] - new_elem) / \
        stds[value], loc=new_elem, scale=stds[value])

        p = p + np.log((invers_distri.cdf(old_elem + 0.5) - invers_distri.cdf(old_elem - 0.5))) - \
        np.log((normal.cdf(new_elem + 0.5 ) - normal.cdf(new_elem - 0.5)))
        return(p)


    def _assymetric_proposal_prob_continuous(stds,
                                             normal,
                                             old_elem,
                                             new_elem,
                                             value,
                                             p):
        """Compute log probability of moving one parameter from new to old point divided by
        probability of moving from old to new for continuous parameters

        :param stds:            standard deviations for each truncated normal distribution given
                                in order of params_order
        :param normal:          truncated normal distribution generated for that element
        :param old_elem:        reference value for that parameter
        :param new_elem:        new value for that parameter
        :param value:           index of parameters from order_params
        :param p:               probability already computed for other parameters
        :return:                log probability
        """

        invers_distri=stats.truncnorm((bounds[value][0] - new_elem) / stds[value], (bounds[value][1] - new_elem) / \
        stds[value], loc=new_elem, scale=stds[value])

        p = p + invers_distri.logpdf(old_elem) - normal.logpdf(new_elem)
        return(p)


    def proposal(ref,stds):
        """Propose new point for Metropolis using a truncated normal distribution centered in the previous
        accepted set and with standard deviation the stds given. It also return the ratio of the propability of
        of moving back divided by the propability to move to the new point in log.
       
        :param ref:             array of preivous accepted set of parameters
        :param stds:            standard deviations for each truncated normal distribution given
                                in order of params_order
        :return:                new point and ratio of probability of moving back to the old point divived by
                                the probability of moving to the old point in log
        """
        new=[]
        p=0
        for key, value in order_params.items():
            if stds[value]==0:
                new.append(ref[value])
            else:
                normal=stats.truncnorm((bounds[value][0] - ref[value]) / stds[value], (bounds[value][1] - ref[value]) / stds[value], loc=ref[value], scale=stds[value])
                if key in['ncomp_aCH','ncomp_aCOH','ncomp_COOH']:
                    elem=round(normal.rvs())
                    new.append(round(elem))
                    p = _assymetric_proposal_prob_discrete(stds, normal, ref[value], elem, value, p)
                else:
                    elem=normal.rvs()
                    new.append(elem)
                    p = _assymetric_proposal_prob_continuous(stds, normal, ref[value], elem, value, p)
        return(new,p)

    

    return(prior, get_sigmas, log_likelihood, prior_uniform, proposal)


def get_functions(run, cluster = None):
    """ Read the model spe file in function of year, site and type, get data and define all useful 
    functions in function of the spe and data
    
    :param run:        name of run
    :param cluster     cluster of interest
    :return            functions: prior, get_sigmas, log_likelihood, prior_uniform, proposal
    """
    model_spe = get_model_spe( run)
    if 'PLS' not in list(model_spe.keys()):
        PLS = 'PLS2'
    else: 
        PLS = model_spe['PLS']
    if 'path_cluster' not in list(model_spe.keys()):
        path_cluster = path_processed_files()['clusters']
    else:
        path_cluster = model_spe['path_cluster']

    
    Y,unc, aCH_, aCOH_, COOH_, aNH2, COO, CO, CONO2 = get_data(PLS, path_cluster, cluster = cluster)

    return _create_functions(aCH_, aCOH_, COOH_, aNH2, COO, CO, CONO2, Y, unc, **model_spe)


