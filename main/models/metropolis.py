from pathlib import Path
import sys
import json
import os


import pandas as pd
import numpy as np
import numpy.random as random

path = Path().resolve()
sys.path.append(f'{path.parents[2]}/')
from main.models.utils import get_functions, check_if_file_exist, get_model_spe
from main.models.paths_results import path_metropolis, path_laplace_LBFGS

_DEFAULT_ITERATIONS = 10**6
_DEFAULT_STDS_CALIBRATION = 3*10**5
_DEFAULT_OUTPUT_RESULTS = 10**6
_DEFAULT_NUMBER_CHAIN = 2
_DEFAULT_INITIAL_STDS = [0.02, 0.02, 0.02, 0.01, 2, 2, 2]
_DEFAULT_BURN_IN = int(0.7*10**5)



def _write_config_to_json(iterations, calibration, output_results, number_of_runs, initial_stds, burn_in, path_results):
    """
    write all the different configurations setting of the metropolis algorithm to the
    configuration file in the run folder

    param iterations:       number of iterations for metropolis
    param calibration:      number of iterations to calibrate the variance of the proposal disitribution
    param output results:   at which frequency to write results to csv
    param number_of_runs:   number of chain run
    inital_stds:            initial variance of the proposal distribution
    burn_in:                number of iterations to remove as burn_in
    path_results:           path of results

    :return: -
    """
    run_config=dict()
    run_config['iterations'] = iterations
    run_config['calibration'] = calibration
    run_config['output_results'] = output_results
    run_config['number_of_runs'] = number_of_runs
    run_config['initial_stds'] = initial_stds
    run_config['burn_in'] = burn_in
    
    with open(path_results['run_config'], 'w') as fp:
        json.dump(run_config, fp, indent=4)
    return




def _write_csv_calibration(final, chain_path, log_lik, model_spe):
    """
    Write csv of calibration chain and re-initialise final the array containing the results

    :param final:       array containing metropolis results until now
    :param chain_path:  in which folder the resuls must be saved
    :param log_lik:     the posterior of the parameters set contained in final
    :param model_spe:    model specificities dictionnary

    :return:            re-initialise final as an empty array
    """
    final = pd.DataFrame(final)
    final['posterior'] = log_lik
    final.columns = list(model_spe['order_params'].keys()) + ['posterior']
    path = os.path.join(chain_path, 'calibration.csv')
    final.to_csv(path, index = False)
    final = []
    return(final)




def _write_csv_output(final, chain_path, log_lik, n, model_spe):
    """
    Write csv of chain and re-initialise final the array containing the results

    :param final:       array containing metropolis results until now
    :param chain_path:  in which folder the resuls must be saved
    :param log_lik:     the loglik of the parameters set contained in final
    :param n:           the iteration we are at
    :param model_spe:    model specificities dictionnary

    :return:            re-initialised final as an empty array
    """
    final=pd.DataFrame(final)
    final['log_lik'] = log_lik
    final.columns = list(model_spe['order_params'].keys()) + ['posterior']
    path = os.path.join(chain_path, str(n)+'.csv')
    final.to_csv(path, index = False)
    final = []
    return(final)




def _get_stds(final, burn_in):
    """
    stds calculated on calibration chain with burn in samples removed

    :param final:       array containing metropolis results until now
    :param burn_in:     number of iterations to be thrown away as burn-in

    :return:            stds to be used for proposal afterwards
    """
    final=pd.DataFrame(final)
    stds = list(2.4**2 / 6 * final.iloc[burn_in:].std().values)
    return(stds)




def _check_if_results_empty(path):
    """
    Check is folder that will contain the results is empty before written out the results

    :param path:    path to folder that will contain the results

    :return:        boolean if empty
    """
    boolean = False
    if not os.listdir(path):
        boolean = True
    return boolean

def _generate_initial_param(run, cluster, model_spe):
    """Takes the best results from laplace to be used as initial vector of params for metropolis

        :param run:         name of run
        :param cluster:     cluster of interest
        :param model_spe:   model_spe dict     

        :return             vector of inital parameter
        """
    model_spe = get_model_spe(run)
    path = path_laplace_LBFGS(run, cluster = cluster)
    path = os.path.join(path, 'results_best.csv')
    if not check_if_file_exist(path):
        raise FileNotFoundError('No laplace results to start from best likelihood point',path)
    df = pd.read_csv(path)
    MAP = df.sort_values('marginal', ascending = False).head(1)
    MAP = MAP[list(model_spe['order_params'].keys())]
    init = list(MAP.values[0])
    return(init)

def metropolis_run(run,
              cluster = None,
              iterations = _DEFAULT_ITERATIONS,
              calibration = _DEFAULT_STDS_CALIBRATION,
              output_results = _DEFAULT_OUTPUT_RESULTS,
              number_of_chain = _DEFAULT_NUMBER_CHAIN,
              initial_stds = _DEFAULT_INITIAL_STDS,
              burn_in = _DEFAULT_BURN_IN,
              ref = None):
    """
    Metropolis run on a cluster
    :param run:                 name of run
    :param cluster:             cluster of interest
    :param iterations:          number of iterations to be run
    :param calibration:         number of iterations to calibrate the standard deviation
                                for the proposal distribution
    :param output_results:      at which frequency to output the results
    :param number_of_runs:      number of chains to be run
    :param initial_stds:        inital standard deviation for proposal distrobution
    :param burn_in:             number of iterations to consider as burn-in and not used
                                to calculated standard deviation
    :parma run:         name to be given to the folder containing the results of the run
    :return:                    -
    """
    print('')

    print('Running Metropolis-Hasting')
    
    prior, get_sigmas, log_likelihood, prior_uniform, proposal = get_functions(run, cluster = cluster)

    path_results = path_metropolis(run, cluster =cluster, number_of_chain = number_of_chain)

    model_spe = get_model_spe(run)
    _write_config_to_json(iterations, calibration, output_results, number_of_chain, initial_stds, burn_in, path_results)





    chain_keys = [key for key in path_results.keys() if 'chain' in key]
    for chain_key in chain_keys:
        print(chain_key)
        print('  ')
        chain_path = path_results[chain_key]
        if _check_if_results_empty(chain_path)==False:
            raise ValueError('Some results are already in:', chain_path)
        if ref is None:
            ref = _generate_initial_param(run, cluster, model_spe)
        final = [ref]
        stds = initial_stds
        log_lik=[log_likelihood(ref) + prior(ref)]
        accepted = 0
        #starts at two because first iteration there is ref + new (2 in one time)
        for n in range(2,iterations+1):
            new, log_prob = proposal(ref, stds)

            a = log_likelihood(new) + prior(new) - log_likelihood(ref) - prior(ref) + log_prob
            if np.log(random.random()) < a:
                log_lik.append(log_likelihood(new) + prior(new))
                final = final + [new]
                accepted = accepted + 1
                ref = new
            else:
                log_lik.append(log_likelihood(ref) + prior(ref))
                final = final + [ref]
            if (n/iterations*100)%5 ==0:
                print(str(int(n/iterations*100))+ '% completed')
            if n>=calibration:
                if n==calibration:
                    stds = _get_stds(final, burn_in)
                    final = _write_csv_calibration(final, chain_path, log_lik, model_spe)
                    log_lik=[]
                elif n%output_results==0:
                    print('Iteration number '+str(n))
                    print('Acceptance rate: ' +str(accepted/n*100))
                    print('  ')
                    final = _write_csv_output(final, chain_path, log_lik, n, model_spe)
                    log_lik=[]

    return

