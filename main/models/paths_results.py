
import os
from pathlib import Path
import sys




def get_results_folder(run, cluster = None ,self_path=None):
    """Get to base of results folder for year, type, site and run
    
    :param run:             name of run    

    :return                 path to result
    """
    if self_path is None:
        self_path = __file__
    
    #Import main path
    current_path = Path(self_path).resolve()
    path = f'{current_path.parents[2]}'
    sys.path.append(path)
    
    results_folder_init = os.path.join(path, 'results')
    results_folder = os.path.join(results_folder_init, run)
    if cluster is not None:
        results_folder = os.path.join(results_folder, str(cluster).zfill(2))

    return results_folder

def create_folder(folder):
    """Create folder if it doesn't exist
    
    :param folder    folder to be created

    :return          nothin
    """
    if not os.path.exists(folder):
        os.makedirs(folder)
    return
        
def path_metropolis(run = 'run1', cluster = None, number_of_chain = 2):
    """Get path of metropolis results
    
    :param name_of_run      name given to the run 
    :param cluster          cluster of interest
    :number_of_chain        number of chain runed
                                   
    :return: dict containing path to
       
       run_folder: folder where all the results from the run will be stored
       run_config: file where the configurations of the run should be written
       chainx: folder where results from chain x will be stored
      
    """
    results_folder = get_results_folder(run, cluster)
    results_folder = os.path.join(results_folder, 'metropolis')
    create_folder(results_folder)
    FILES = {
        'run_folder': results_folder
    }
    FILES['run_config'] = os.path.join(results_folder, 'run_config.json')

    for n in range(1, number_of_chain + 1):
        chain = 'chain'+str(n)
        FILES[chain] = os.path.join(results_folder, chain)
        create_folder(os.path.join(results_folder, chain))

    return(FILES)

def path_laplace_LBFGS(run, cluster = None, variable = 'ncomps', self_path=None):
    """Get path of metropolis results

    :param name_of_run      name given to the run
    :param cluster          cluster of interest 
    :param variable         variable for which we want to  get the posteriori distribution
                                          
    :return run_folder:    folder where all the results from the run will be stored
    """
    results_folder = get_results_folder( run, cluster= cluster, self_path=self_path)
    results_folder = os.path.join(results_folder, 'laplace_LBFGS')
    run_folder = os.path.join(results_folder, variable)
    
    create_folder(run_folder)

    return(run_folder)
