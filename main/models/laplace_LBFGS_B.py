from pathlib import Path
import sys
import json
import os


import pandas as pd
import numpy as np
import scipy
from itertools import product

import copy


path = Path().resolve()
sys.path.append(f'{path.parents[2]}/')
from main.models.utils import get_functions, get_model_spe
from main.models.paths_results import path_laplace_LBFGS

import numdifftools as nd

# In[2]:


_DEFAULT_MAX_DIFF_MARGINAL = 20

_DEFAULT_BNDS_LAPLACE = [[0.5, 1], [0, 1], [0, 1], [0, 1]]
_DEFAULT_STEPS_LAPLACE = [0.001, 0.001, 0.005, 0.001]



_DEFAULT_STARTING_POINT = [0.75, 0.48, 0.4, 0.018]



def _get_ncomps_lims(run):
    """_get_bounds for ncomps, so that we find the MAP for every possible set of ncomps
    
    :param run:    name of run

    :return:        bounds for each fg
    """
    model_spe = get_model_spe(run)
    order_params = model_spe['order_params']
    bnds= model_spe['bounds']

    bnds_ncomp_aCH = bnds[order_params['ncomp_aCH']]
    bnds_ncomp_aCOH = bnds[order_params['ncomp_aCOH']]
    bnds_ncomp_COOH = bnds[order_params['ncomp_COOH']]

    bnds_ncomp_aCH = [x+0.5 for x in bnds_ncomp_aCH]
    bnds_ncomp_aCOH = [x+0.5 for x in bnds_ncomp_aCOH]
    bnds_ncomp_COOH = [x+0.5 for x in bnds_ncomp_COOH]

    return bnds_ncomp_aCH, bnds_ncomp_aCOH, bnds_ncomp_COOH

def _get_bounds(run):
    """_get_bounds and order of params for specifiy year and type, the bounds will be modified to not include the bounds
    for the number of components, the lower bounds for kappa will be set to 10**-16 for numerical reason
    (as the prior can return infinity if 0), finally the bounds are defined in a tuple instead of an array
    
    :param run:    name of run

    :return:        bounds and order_params a dict that index the bounds to the specific parameters
    """
    model_spe = get_model_spe(run)
    order_params = model_spe['order_params']
    bnds= model_spe['bounds']
    if 'kappa' in order_params.keys():
        bnds[order_params['kappa']][0] = 10**-16
    bnds = [tuple(x) for x in bnds]
    bnds = bnds[0:len(bnds)-3]
    return bnds, order_params




# In[5]:


def _generate_starting_point(generate_initial_param):
    """Generate starting point for the optimize function randomly using the priors 
    and remove the ones generated for the ncomp

    :param: function that generate_intial_parm for all parameters
    
    :return: tuple of starting point (all params except ncomps)
    """
    init_ = generate_initial_param()
    init_ = init_[0:len(init_)-3]
    return(init_)


# In[6]:


def _transform_to_tuple(elem):
    """ Transfer vector to tuple
    param elem:     vector

    return elem:    tuple
    """
    elem = tuple(elem)
    return elem


# In[7]:


def _remove_parameter_of_interest(elem, variable, order_params):
    """ Remove parameter that is fixed from starting_point

    param elem: elem a vector containing starting point
    param variable: parameters that is fixed
    param order_params: dict indexing the order of parameters in elem

    return: starting point without fixed parameters
    """
    del elem[order_params[variable]]
    return elem


# In[8]:


def _insert_ncomps(vector_, ncomp_aCH, ncomp_aCOH, ncomp_COOH, order_params):
    """ 
    insert ncomps (discrete) parameters (that are fixed when optimizing) in the vector containing
    continuous parameters.

    param vector_:          vector containing continuous parameters values
    param ncomp_aCH:        # of components-1 used to predict aCH
    param ncomp_aCOH:       # of components-1 used to predict aCOH
    param ncomp_COOH:       # of components-1 used to predict COOH
    param order_params:     dict indexing the order of parameters in elem

    return vector:          vector containing continuous and discrete (ncomps) params


    """
    vector_ = list(vector_)
    vector_.insert(order_params['ncomp_aCH'], ncomp_aCH)
    vector_.insert(order_params['ncomp_aCOH'], ncomp_aCOH)
    vector_.insert(order_params['ncomp_COOH'], ncomp_COOH)
    return vector_


# In[9]:


def _insert_parameter_of_interest(vector_, order_params, variable, value_):
    """ Insert parameter of interest (fixed parameter when optimizing) in vector with the optimized parameters.
    
    param vector_:          vector containing other parameters values
    param order_params:     dict indexing the order of parameters in elem
    param variable:         name of variable of interest
    param value_ :          value of parameter of interest

    return:                 vector containing parameters values
    """
    vector_.insert(order_params[variable], value_)
    return vector_
    


# In[10]:


def _to_be_minimized(vector_, prior, log_likelihood, ncomp_aCH, ncomp_aCOH, ncomp_COOH, variable, order_params, value_= None):
    """The function to be minimized takes as input a vector containing the parameters
    to be minimized and computes the posterior distribution
    
    :param vector_:                    vector containing the parameters to be minimized
    :param prior:                      prior function
    :param log_likelihood:             log_likelihood function
    :param ncomp_aCH:                  number of latent variables used to predict aCH
    :param ncomp_aCOH:                 number of latent variables used to predict aCOH
    :param ncomp_COOH:                 number of latent variables used to predict COOH
    :param variable:                   the variable that is fixed
    :order_params:                     dict indexing of the order of parameters in vector_
    :value_:                           value of the fixed variable if it is not the number of latent variables
    
    :return: posterior probability
    """
    vector_ = _insert_ncomps(vector_, ncomp_aCH, ncomp_aCOH, ncomp_COOH, order_params)
    if variable !='ncomps':
        _insert_parameter_of_interest(vector_, order_params, variable, value_)
    value = prior(vector_)+ log_likelihood(vector_)
    return(-value)


# In[11]:


def _get_square_root_determinant(sol, dim):
    """Calculate the square root determinant from the hessian matrix

    :param sol:     hessian matrix
    :parm dim:      number of parameters being optimized

    return:         square root of the determinant of the hessian matrix, 
                    nan if the hessian is not semi-positive definite
    """
    try:
        chol = np.linalg.cholesky(sol)
        det = np.sum(np.log(np.diag(chol)))
    
    except Exception:
        det = np.nan
    return(det)

# In[12]:


def _get_reduced_version(df, variable):
    """ get reduced version of dataframe just the parameter value for which we want to get the posterior
    and the marginal posterior and the square root of the determinant

    param df:            full dataframe, includes also the optimiized value of the other parameters
    param variable:      variable for which we are estimating the posterior distribution

    return df:          reduced version of the dataframe         
    """
    list_of_columns = ['marginal','posterior','det_hess']
    diff = []
    index = 0
    for column in list_of_columns:
        diff.append(10 - np.max(df[column]))
        df[column] = df[column] + diff[index]
        df[column] = df[column].apply(lambda x: np.exp(x))
        index = index + 1
    df = df.groupby(variable).sum()
    index = 0
    df = df[list_of_columns]
    
    for column in list_of_columns:
        df[column] = df[column].apply(lambda x: np.log(x))
        df[column] = df[column].apply(lambda x: x-diff[index])
        index = index + 1
    return df

def _function_for_hessian(prior, log_likelihood, ncomp_aCH, ncomp_aCOH, ncomp_COOH, variable, order_params, value_= None):
    """ return a function to get the hessian for numdifftoos

    param vector_:                  continuous parameters point at which we want the hessian

    return function_for_hessian:    function to be able to calculate the hessian from numdifftools
    """

    def function_for_hessian(vector_):
        vector_ = _insert_ncomps(vector_, ncomp_aCH, ncomp_aCOH, ncomp_COOH, order_params)
        if variable !='ncomps':
            _insert_parameter_of_interest(vector_, order_params, variable, value_)
        value = prior(vector_)+ log_likelihood(vector_)
        return -value
    return function_for_hessian            


# In[15]:


def calculate_best_ncomps(run, cluster, max_diff_marginal):
    """ Keep only the best set of number of components that have a probability that is not lower
    than max_diff_marginal from the best solution and save the file

    param run:                  name of run
    param cluster:              cluster of interest
    param max_diff_marginal:    limit until which we keep the solutions as non negligeable

    return -

    """
    path = os.path.join(path_laplace_LBFGS(run, cluster=cluster), 'results.csv')
    final = pd.read_csv(path)
    max_diff_marginal_ = max_diff_marginal* np.log(10)
    to_keep = final[final['marginal']> np.max(final['marginal'])-max_diff_marginal]
    path = os.path.join(path_laplace_LBFGS(run, cluster=cluster), 'results_best.csv')
    to_keep.to_csv(path, index = False)
    return



# In[16]:


def read_best_ncomps_file(run, cluster = None):
    """ read results file containing only the best set of ncomps

    param run:          name of run
    param cluster:      cluster of interest

    return df:          dataframe containing all the set of number of components that gives non negligeable marginal

    """
    path = os.path.join(path_laplace_LBFGS(run, cluster = cluster), 'results_best.csv')
    df = pd.read_csv(path)
    df = df[['ncomp_aCH', 'ncomp_aCOH', 'ncomp_COOH']]
    return df


def get_columns_name(variable, order_params):
    """ Get the columns name of the results csv in function of which variable we are estimating the posterior distribution for

    param variable:         variable we are estimating the posterior distribution for
    param order_params:     dict indexing the order of parameters 
    """
    dict_temp = copy.deepcopy(order_params)
    for fg in ['ncomp_aCH','ncomp_aCOH','ncomp_COOH']:
        dict_temp.pop(fg)
    dict_temp.pop(variable)
    names = list(dict_temp.keys()) + ['ncomp_aCH','ncomp_aCOH','ncomp_COOH'] + [variable] + ['posterior', 'det_hess', 'marginal']
    return(names)
    

def laplace(run, cluster, variable, ncomps, starting_point, bnds_laplace = None, steps_laplace = None):##ncomps
    """ find maximum posterior for each set of fixed number of components and save that 
    result in a csv

    :param year:    year
    :param site:    site
    :param type_:   type (urban, rural or all)

    return -
    """

    prior, get_sigmas, log_likelihood, prior_uniform, proposal = get_functions(run, cluster)
    
    bnds, order_params = _get_bounds(run)    
    starting_point_ = copy.deepcopy(starting_point)
    if variable !='ncomps':
        values = np.arange(bnds_laplace[order_params[variable]][0], bnds_laplace[order_params[variable]][1],
                    steps_laplace[order_params[variable]])
        bnds = _remove_parameter_of_interest(bnds, variable, order_params)
        starting_point_ = _remove_parameter_of_interest(starting_point_, variable, order_params)
        col_names = get_columns_name(variable, order_params)

    else:
        values = [None]
        col_names = list(order_params.keys())+ ['posterior', 'det_hess', 'marginal']  


    bnds = _transform_to_tuple(bnds)
    starting_point_ = _transform_to_tuple(starting_point_)

    all_=[]
    for index, row in ncomps.iterrows():
            ncomp_aCH = row['ncomp_aCH']
            ncomp_aCOH = row['ncomp_aCOH']
            ncomp_COOH = row['ncomp_COOH']
            for value_ in values:
                ncomps_value= [ncomp_aCH, ncomp_aCOH, ncomp_COOH]
                sol=scipy.optimize.minimize(_to_be_minimized, starting_point_, method='L-BFGS-B',bounds=bnds,
                                           args =(prior, log_likelihood, ncomp_aCH, ncomp_aCOH, ncomp_COOH,
                                                  variable, order_params,value_))
                if np.array_equal(sol.x, starting_point_) or sol.status !=0:
                    if  ~np.isfinite(bnds).all():
                        sol=scipy.optimize.minimize(_to_be_minimized, starting_point_, method='BFGS',
                        args =(prior, log_likelihood, ncomp_aCH, ncomp_aCOH, ncomp_COOH,
                        variable, order_params,value_))
                continuous = list(sol.x)
                function_for_hessian = _function_for_hessian(prior, log_likelihood, ncomp_aCH, ncomp_aCOH, ncomp_COOH, variable, order_params, value_= value_)
                fun_ = nd.Hessian(function_for_hessian, method = 'complex')
                hess = fun_(continuous)
                log_lik = [ -sol.fun - _get_square_root_determinant(hess,len(continuous))]
                if variable =='ncomps':
                    vector_ = continuous + ncomps_value + [-sol.fun] + [-_get_square_root_determinant(hess,len(continuous))] + log_lik
                else:
                    vector_ = continuous + ncomps_value + [value_] + [-sol.fun] + [-_get_square_root_determinant(hess,len(continuous))] + log_lik
                all_.append(vector_)

    final = pd.DataFrame(all_)
    final.columns = col_names  
    path = os.path.join(path_laplace_LBFGS(run, cluster = cluster, variable = variable), 'results.csv')
    final.to_csv(path, index = False)
    
    if variable == 'ncomps':
        for ncomp_element in ['ncomp_aCH','ncomp_aCOH','ncomp_COOH']:
            final_ = copy.deepcopy(final)
            temp = _get_reduced_version(final_, ncomp_element)
            path = os.path.join(path_laplace_LBFGS(run, cluster = cluster ,variable = ncomp_element), 'posterior.csv')
            temp.to_csv(path)
    else:
        temp = _get_reduced_version(final, variable)
        path = os.path.join(path_laplace_LBFGS(run, cluster = cluster, variable = variable), 'posterior.csv')
        temp.to_csv(path)

    return 

def get_list_of_parameters(run):
    """ Get list of parameters for which we have to get the posterior distribution. All the parameters 
    listed in model_spe['order_params]

    param run:                      name of run

    return list_of_parameters:      list of parameters for which we want to estimate the posterior distribution

    """
    model_spe = get_model_spe(run)
    list_of_parameters = list(model_spe['order_params'].keys())
    list_of_parameters.remove('ncomp_aCH')
    list_of_parameters.remove('ncomp_aCOH')
    list_of_parameters.remove('ncomp_COOH')
    return list_of_parameters

def save_run_config(run, cluster, starting_point, bnds_laplace, steps_laplace, max_diff_marginal):
    """save run configuration file that will specified all the settings of this laplace estimatiion

    param run:              name of run
    param cluster:          cluster of interest
    param starting_point:   starting point of the optimization
    bnds_laplace:           bounds between which the parameters posterior is estimated
    steps_laplace:          at which resolution the posterior is estimated
    max_diff_marginal:      the log value difference under which a set of number of latent variables is considered negligeable

    """
    run_config = dict()
    run_config['run'] = run
    run_config['starting_point'] = starting_point
    run_config['bnds_laplace'] =bnds_laplace 
    run_config['steps_laplace'] = steps_laplace
    run_config['max_diff_marginal'] = max_diff_marginal
    path_to_save = path_laplace_LBFGS(run, cluster = cluster, variable = 'ncomps')
    path_to_save = Path(path_to_save).parents[0]
    path_to_save = os.path.join(path_to_save, 'run_config.json')
    with open(path_to_save, 'w') as fp:
        json.dump(run_config, fp, indent=4)
    return 




def expand_grid(bnds_ncomp_aCH, bnds_ncomp_aCOH, bnds_ncomp_COOH):
    """ From bounds of number of latent variables create dataframe with every possible set
    of number of latent variables

    params bnds_ncomp_aCH:          bounds for the number of latent variables used to predict aCH
    params bnds_ncomp_aCOH:         bounds for the number of latent variables used to predict aCOH
    params bnds_ncomp_COOH:         bounds for the number of latent variables used to predict COOH

    return: dataframe containing all the possible set of number of latent variables
    """
    dict_bnds = dict()
    dict_bnds['ncomp_aCH'] = list(range(int(bnds_ncomp_aCH[0]), int(bnds_ncomp_aCH[1])))
    dict_bnds['ncomp_aCOH'] = list(range(int(bnds_ncomp_aCOH[0]), int(bnds_ncomp_aCOH[1])))
    dict_bnds['ncomp_COOH'] = list(range(int(bnds_ncomp_COOH[0]), int(bnds_ncomp_COOH[1])))
    return pd.DataFrame([row for row in product(*dict_bnds.values())], 
                       columns=dict_bnds.keys())

def laplace_LBFGS_B_run(run,
        cluster = None,
        starting_point = _DEFAULT_STARTING_POINT,
        bnds_laplace = _DEFAULT_BNDS_LAPLACE,
        steps_laplace = _DEFAULT_STEPS_LAPLACE,
        max_diff_marginal = _DEFAULT_MAX_DIFF_MARGINAL
        ):
    """ Run laplace estimation for all possible set of number of components and for
    all continuous variables listed in model_spe file. Write the results to a laplace_LBFGS folder

    param run:          name of run
    param cluster:      cluster of interest
    param starting_point:   starting point of the optimization process
    bnds_laplace:           bounds between which the parameters posterior is estimated
    steps_laplace:          at which resolution the posterior is estimated
    max_diff_marginal:      the log value difference under which a set of number of latent variables is considered negligeable

    return
    """
    print('Running laplace L-BFGS-B')
    # First find MAP for every possible set of ncomps
    print('Variable: ncomps')
    bnds_ncomp_aCH, bnds_ncomp_aCOH, bnds_ncomp_COOH =  _get_ncomps_lims(run)
    ncomps = expand_grid(bnds_ncomp_aCH, bnds_ncomp_aCOH, bnds_ncomp_COOH)
    laplace(run, cluster, 'ncomps',ncomps, starting_point)


    #Get best ncomps based on max_diff_margin and write run_config file
    save_run_config(run, cluster, starting_point, bnds_laplace, steps_laplace, max_diff_marginal)
    calculate_best_ncomps(run, cluster, max_diff_marginal)
    ncomps = read_best_ncomps_file(run, cluster = cluster)
    
    # Run laplace for all the continous parameters over the best possible set of ncomèps
    list_of_parameters = get_list_of_parameters(run)
    for variable in list_of_parameters:
        print('Variable: '+ variable)
        laplace(run, cluster, variable, ncomps, starting_point, bnds_laplace, steps_laplace)
    return




