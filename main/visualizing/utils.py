## Need to be re-written in more concise way.
import numpy as np
import numpy.random as random
import pandas as pd

import scipy.stats as stats

from datetime import datetime
import re
import math
import os
from pathlib import Path
import sys
import json
self_path = __file__

current_path = Path(self_path).resolve()
path = f'{current_path.parents[1]}'
sys.path.append(path)

import matplotlib.pyplot as plt

from main.paths import  path_model_spe, path_processed_files,  path_model_spe

from main.models.utils import get_data, check_if_file_exist, get_arrays_ncomp, read_json, _check_content_model_spe, molar_masses, lambdas_other_FGs


from main.models.paths_results import path_metropolis


titles_for_plots = dict()
titles_for_plots['alpha'] = r'$\alpha$'
titles_for_plots['b1'] = r'$\lambda_{C, aCH}$'
titles_for_plots['b3'] = r'$\lambda_{C, aCOH}$'
titles_for_plots['kappa'] = r'$\kappa^2$'
titles_for_plots['ncomp_aCH'] = r'$k_{aCH}$'
titles_for_plots['ncomp_aCOH'] = r'$k_{aCOH}$'
titles_for_plots['ncomp_COOH'] = r'$k_{COOH}$'
titles_for_plots['marginal'] = 'Marginal'
titles_for_plots['posterior'] = 'Posterior'
titles_for_plots['det_hess'] = 'Det. Hess.'


lambdas_O = dict()
lambdas_O['aCH'] = 0
lambdas_O['aCOH'] = 1
lambdas_O['COOH'] = 2
lambdas_O['CO'] = 1.5
lambdas_O['CONO2'] = 0
lambdas_O['aNH2'] = 0
lambdas_O['COO'] = 2

lambdas_H = dict()
lambdas_H['aCH'] = 1
lambdas_H['aCOH'] = 1
lambdas_H['COOH'] = 1
lambdas_H['CO'] = 0
lambdas_H['CONO2'] = 0
lambdas_H['aNH2'] = 1
lambdas_H['COO'] = 0


lambdas_N = dict()
lambdas_N['aCH'] = 0
lambdas_N['aCOH'] = 0
lambdas_N['COOH'] = 0
lambdas_N['CO'] = 0
lambdas_N['CONO2'] = 0
lambdas_N['aNH2'] = 0.5
lambdas_N['COO'] = 0


def get_type(index_):
    """ get type as urban rural in function of index

    param index_:       index in form SITE_YYYYMMDD

    return type_:       urban or rural
    """

    sitecode =  index_.split('_')[0]
    if sitecode in ['BIRM1', 'BYIS1', 'PHOE1', 'FRES1', 'PHOE5', 'PUSO1', 'PHX5A', 'PHX1A']:
        type_ = 'urban'
    else:
        type_ = 'rural'
    return (type_)

def _read_run_config(path_chain):
    path_run = Path(path_chain).parents[0]
    run_config = read_json(os.path.join(path_run, 'run_config.json'))
    return(run_config)
    

def _get_number_of_chain(chain):
    number = int(re.search(r'\d+', chain).group())
    return number



def _dataframe_results(path, config_run):
    file = str(config_run['iterations'])+'.csv'
    frame = pd.read_csv(os.path.join(path, file))
    return frame
        


def get_results_metropolis(run, chain, cluster= None):
    number_of_chain = _get_number_of_chain(chain)
    paths = path_metropolis(run = run, cluster = cluster, number_of_chain = number_of_chain)
    path = paths[chain]
    config_run = _read_run_config(path)
    df = _dataframe_results(path, config_run)
    return df




class LaTeX_template:

    def __init__(self, fileobj):

        self.fileobj = fileobj

    def write(self, content):

        self.fileobj.write(content + '\n\n')

    def preamble(self):

        preamble = r'''
\documentclass{article}
\usepackage[margin=2.5cm]{geometry}
\usepackage{parskip}
\usepackage{graphicx}
\usepackage{booktabs}
        '''
        self.write(preamble)

    def begin(self):

        self.write(r'\begin{document}')

    def end(self):

        self.write(r'\end{document}')

    def write_section(self, section_name):
        self.write(r'\section{{{}}}'.format(section_name))

    def includegraphics(self, filename, options=r'width=.5\textwidth'):
        if os.name == 'nt':
            filename = filename.replace('\\', '/')
        self.write(r'\centerline{{\includegraphics[{}]{{{}}}}}'.format(options, filename))
    
    def include_table(self, df):
        self.write(r'\begin{center}')
        self.write(df.to_latex())
        self.write(r'\end{center}')




def _handle_ticks(axes_, params_to_be_plotted):
    """handle ticks for parameters plot

    :param axes_:                   axes of metropolis plot
    :param params_to_be_plotted:    all the parameters to be plotted

    :return: -
    """
    for n in range(0,len(params_to_be_plotted)):
        axes_[n].set_yticks([], [])
        if params_to_be_plotted[n] in ['ncomp_aCH', 'ncomp_aCOH', 'ncomp_COOH']:
            axes_[n].set_xlim([0,30])

    return



def plot_uniform(variable, model_spe, axes_n):
    """Plot uniform priors

    :param variable:    variable for which the prior is
    :param model_spe:   model specificities of the run
    :param axes_n:       axes on which to plot


    :return: -
    """
    index = model_spe['order_params'][variable]
    low, high = model_spe['bounds'][index]
    if variable in ['ncomp_aCH', 'ncomp_aCOH', 'ncomp_COOH']:
        low = low + 1.5
        high = high + 1.5

    under, upper = axes_n.get_ylim()
    rect = plt.Rectangle((low,0), high-low, upper *0.95, angle=0.0, color='black', fill = False,linewidth =2)
    axes_n.add_patch(rect)
    diff = high-low
    low = low - diff*0.04
    high = high + diff*0.04
    axes_n.set_xlim([low, high])
    return


# In[39]:


def plot_continuous(variable, model_spe, axes_n):
    """Plot continuous priors: can be Weibull, normal or inverse gamma

    :param variable:    variable for which the prior is
    :param model_spe:   model specificities of the run
    :param axes_n:       axes on which to plot

    :return: -
    """
    prior = model_spe['prior_'+variable]
    if prior['type'] =='normal':
        distri = stats.norm(prior['mean'], prior['std'])
    if prior['type']=='weibull':
        distri = stats.weibull_min(c=prior['shape'],scale=prior['scale'])
    if prior['type']=='inverse_gamma':
        distri = stats.gamma(a=prior['shape'], scale=prior['scale'])
    if prior['type']=='beta':
        distri = stats.beta(a = prior['shape1'], b= prior['shape2'])

    low = distri.ppf(0.001)
    up = distri.ppf(0.999)
    low = 0
    up = 1
    x = np.arange(low, up, 0.001)
    if prior['type']=='beta':
        x_prime = (x- prior['a'])/(prior['b']-prior['a'])
        pdf = distri.pdf(x_prime)*(prior['b']-prior['a'])
        axes_n.plot(x, pdf, color='black')
        t1 = next((i for i, x in enumerate(pdf) if x), None) 
        t2 = next((i for i, x in reversed(list(enumerate(pdf))) if x), None) 
        axes_n.axvline(x=x[t1], color = 'red')
        axes_n.axvline(x=x[t2], color = 'red')
    else:
        axes_n.plot(x, distri.pdf(x), color='black')
    return 


# In[40]:
def plot_prior_ncomps(variable, model_spe, axes_n):
    """Plot discrete priors for ncomps

    :param variable:    variable for which the prior is
    :param model_spe:   model specificities of the run
    :param axes_n:       axes on which to plot

    :return: -
    """
    prior_ncomp = model_spe['prior_'+variable]
    ncomp = np.arange(1,len(prior_ncomp)+0.6,1)
    axes_n.hist(ncomp, weights = np.exp(prior_ncomp), bins =np.linspace(0.6,31.6,32), density = True, color='black',alpha=0.3)
    return


def plot_priors(run, params_to_be_plotted, axes_, model_spe):
    """Central function to plot all priors on metropolis plot

    :param run:                     name of run
    :param params_to_be_plotted:    all the parameters to be plotted
    :param axes:                    axes of metropolis plot

    :return: -
    """
    model_spe = get_model_spe(run)
    for n in range(0, len(params_to_be_plotted)):
        variable = params_to_be_plotted[n]
        if('prior_'+variable in list(model_spe.keys())):
            if variable in ['ncomp_aCH','ncomp_aCOH','ncomp_COOH']:
                plot_prior_ncomps(variable, model_spe, axes_[n])
            else:
                pdf = plot_continuous(variable, model_spe, axes_[n])
        elif variable !='posterior':
            plot_uniform(variable, model_spe, axes_[n])
            
    return


def get_season(df):
    datetime_ = df.index.to_series().apply(lambda x: x.split('_')[1])
    datetime_ =  datetime_.apply(lambda x: datetime.strptime(x, '%Y%m%d'))
    df['season'] =  datetime_.apply(lambda x: (x.month%12 + 3)//3)
    return df


def get_sigmas(kappa, Y, kappa_intercept):
    """Get sigmas in function of kappa and OC

        :param kappa:    kappa
        :param Y:        OC values

        :return: array of uncertainties
        """
    sigmas = (kappa_intercept+kappa*Y**2)**0.5
    return sigmas

def get_model_spe(run):
    """ Get model spe
    param run:             name of run

    return model_spe:      dict containing model specifications
    """
    model_spe_path = path_model_spe(run)
    if check_if_file_exist(model_spe_path)==False:
        print(model_spe_path)
        raise FileNotFoundError('Model_spe file doesnt exist')
    model_spe = read_json(model_spe_path)
    _check_content_model_spe(model_spe)
    return model_spe


def get_constituents(run, parameters, cluster = None):
    """ Read the appropriate data in function of name of run and cluster and return predictions
    using given parameters values
    
    :param run:         name of run
    :param parameters:  parameters to be used to make prediction
    :param cluster:     cluster of interest
    :return             dataframe containing all predictions
    """

    model_spe = get_model_spe(run)
    order_params = model_spe['order_params']
    if 'PLS' not in list(model_spe.keys()):
        PLS = 'PLS2'
    else:
        PLS = model_spe['PLS']

    if 'path_cluster' not in list(model_spe.keys()):
        path_cluster = path_processed_files()['clusters']
    else:
        path_cluster = model_spe['path_cluster']

    Y,unc, aCH_, aCOH_, COOH_, aNH2, COO, CO, CONO2, data = get_data(PLS, path_cluster,  cluster = cluster, return_df = True)
    results = data.copy()


    ## Get useful informations such as season type of site:
    data = get_season(data)
    data['type'] = data.index.to_series().apply(lambda x: get_type(x))
    data['SiteCode'] = data.index.to_series().apply(lambda x: x.split('_')[0])

    
    aCH = aCH_[0, int(parameters[order_params['ncomp_aCH']])]

    aCOH = aCOH_[0, int(parameters[order_params['ncomp_aCOH']])]

    COOH = COOH_[0, int(parameters[order_params['ncomp_COOH']])]

    results['aCH'] = aCH
    results['aCOH'] = aCOH
    results['COOH'] = COOH
    results['aNH2'] = aNH2
    results['COO'] = COO
    results['CONO2'] = CONO2
    results['CO'] = CO

    results = results.drop(['ncomp', 'variable', 'OCf:Value', 'OCf:Unc'],1)


## Elements abudance in moles

    def get_O():
        O = lambdas_O['aCH']*aCH + lambdas_O['aCOH']* aCOH + lambdas_O['COOH'] * COOH + lambdas_O['CO'] * CO + lambdas_O['CONO2'] * CONO2 + lambdas_O['aNH2'] *aNH2 + lambdas_O['COO'] *COO
        return (O)
    
    def get_C():
        C = (parameters[order_params['b1']]*aCH + parameters[order_params['b3']]*aCOH + COOH
         + lambdas_other_FGs['aNH2'] * aNH2 + lambdas_other_FGs['COO'] * COO 
         + lambdas_other_FGs['CO'] * CO + lambdas_other_FGs['CONO2'] *CONO2)
        return (C)
    
    def get_H():
        H = lambdas_H['aCH']*aCH + lambdas_H['aCOH'] *aCOH + lambdas_H['COOH'] * COOH + lambdas_H['CO'] * CO + lambdas_H['CONO2'] * CONO2 + lambdas_H['aNH2'] *aNH2 + lambdas_H['COO'] *COO
        return (H) 
    
    def get_N():
        N = lambdas_N['aCH']*aCH + lambdas_N['aCOH']* aCOH + lambdas_N['COOH'] * COOH + lambdas_N['CO'] * CO + lambdas_N['CONO2'] * CONO2 + lambdas_N['aNH2'] *aNH2 + lambdas_N['COO'] *COO
        return N 
    
## OC for each FG
    def get_OC_aCH():
        OC_aCH = parameters[order_params['b1']]*aCH * molar_masses['C']
        return OC_aCH
    
    def get_OC_aCOH():
        OC_aCOH = parameters[order_params['b3']]*aCOH * molar_masses['C']
        return OC_aCOH
    
    def get_OC_COOH():
        OC_COOH = lambdas_other_FGs['COOH'] * COOH * molar_masses['C']
        return OC_COOH

    def get_OC_aNH2():
        OC_aNH2 = lambdas_other_FGs['aNH2'] * aNH2 * molar_masses['C']
        return OC_aNH2

    def get_OC_COO():
        OC_COO = lambdas_other_FGs['COO'] * COO  * molar_masses['C']
        return OC_COO

    def get_OC_CO():
        OC_CO = lambdas_other_FGs['CO'] * CO  * molar_masses['C']
        return OC_CO
    
    def get_OC_CONO2():
        OC_CONO2 = lambdas_other_FGs['CONO2'] * CONO2 * molar_masses['C']
        return OC_CONO2

##OM sans C for each FG
    
    def get_OM_aCH_sans_C():
        OM_aCH_sans_C = aCH * (lambdas_H['aCH'] * molar_masses['H'] + lambdas_O['aCH'] * molar_masses['O'] + lambdas_N['aCH'] * molar_masses['N'])
        return OM_aCH_sans_C
    
    def get_OM_aCOH_sans_C():
        OM_aCOH_sans_C = aCOH * (lambdas_H['aCOH'] * molar_masses['H'] + lambdas_O['aCOH'] * molar_masses['O'] + lambdas_N['aCOH'] * molar_masses['N'])
        return OM_aCOH_sans_C
  
    def get_OM_COOH_sans_C():
        OM_COOH_sans_C = COOH * (lambdas_H['COOH'] * molar_masses['H'] + lambdas_O['COOH'] * molar_masses['O'] + lambdas_N['COOH'] * molar_masses['N'])
        return OM_COOH_sans_C

    def get_OM_aNH2_sans_C():
        OM_aNH2_sans_C = aNH2 * (lambdas_H['aNH2'] * molar_masses['H'] + lambdas_O['aNH2'] * molar_masses['O'] + lambdas_N['aNH2'] * molar_masses['N'])
        return OM_aNH2_sans_C
        
    def get_OM_COO_sans_C():
        OM_COO_sans_C = COO * (lambdas_H['COO'] * molar_masses['H'] + lambdas_O['COO'] * molar_masses['O'] + lambdas_N['COO'] * molar_masses['N'])
        return OM_COO_sans_C

    def get_OM_CO_sans_C():
        OM_CO_sans_C = CO * (lambdas_H['CO'] * molar_masses['H'] + lambdas_O['CO'] * molar_masses['O'] + lambdas_N['CO'] * molar_masses['N'])
        return OM_CO_sans_C

    def get_OM_CONO2_sans_C():
        OM_CONO2_sans_C = CONO2 * (lambdas_H['CONO2'] * molar_masses['H'] + lambdas_O['CONO2'] * molar_masses['O'] + lambdas_N['CONO2'] * molar_masses['N'])
        return OM_CONO2_sans_C

## OM for each FG
    
    def get_OM_aCH():
        OM_aCH = get_OM_aCH_sans_C() + get_OC_aCH()
        return OM_aCH
    
    def get_OM_aCOH():
        OM_aCOH = get_OM_aCOH_sans_C() + get_OC_aCOH()
        return OM_aCOH
    
    def get_OM_COOH():
        OM_COOH = get_OM_COOH_sans_C() + get_OC_COOH()
        return OM_COOH
    
    def get_OM_aNH2():
        OM_aNH2 = get_OM_aNH2_sans_C() + get_OC_aNH2()
        return OM_aNH2
    
    def get_OM_COO():
        OM_COO = get_OM_COO_sans_C() + get_OC_COO()
        return OM_COO
    
    def get_OM_CO():
        OM_CO = get_OM_CO_sans_C() + get_OC_CO()
        return OM_CO
    
    def get_OM_CONO2():
        OM_CONO2 = get_OM_CONO2_sans_C() + get_OC_CONO2()
        return OM_CONO2

## Get total OM and OC
        
    def get_OC():
        OC = get_C()* molar_masses['C'] 
        return (OC)
    def get_OM_sans_C():
        OM_sans_C = get_O()* molar_masses['O'] + get_H() * molar_masses['H'] + get_N() * molar_masses['N']
        return OM_sans_C
    
    def get_OM():
        OM = get_OM_sans_C() + get_OC() 
        return (OM)
    


    def get_likelihood():
        if 'kappa' in list(model_spe['order_params'].keys()):

            sigmas = get_sigmas(parameters[order_params['kappa']], Y, model_spe['kappa_intercept'])
        else:
            sigmas = unc

        OC_pred = get_OC() / parameters[order_params['alpha']]
        Ls=-0.5*((Y-OC_pred)**2/(sigmas**2)+np.log(sigmas**2*2*math.pi))
        return Ls

    def get_r():
        if 'kappa' in list(model_spe['order_params'].keys()):
            sigmas = get_sigmas(parameters[order_params['kappa']], Y,model_spe['kappa_intercept'])
        else:
            sigmas = unc
        OC_pred = get_OC() / parameters[order_params['alpha']]
        r = (Y-OC_pred)/sigmas
        return r



    

    data['OC_aCH'] = get_OC_aCH()
    data['OC_aCOH'] = get_OC_aCOH()
    data['OC_COOH'] = get_OC_COOH()
    data['OC_aNH2'] = get_OC_aNH2()
    data['OC_COO'] = get_OC_COO()
    data['OC_CO'] = get_OC_CO()
    data['OC_CONO2'] = get_OC_CONO2()

    data['OCalpha_aCH'] = get_OC_aCH()/parameters[order_params['alpha']]
    data['OCalpha_aCOH'] = get_OC_aCOH()/parameters[order_params['alpha']]
    data['OCalpha_COOH'] = get_OC_COOH()/parameters[order_params['alpha']]
    data['OCalpha_aNH2'] = get_OC_aNH2()/parameters[order_params['alpha']]
    data['OCalpha_COO'] = get_OC_COO()/parameters[order_params['alpha']]
    data['OCalpha_CO'] = get_OC_CO()/parameters[order_params['alpha']]
    data['OCalpha_CONO2'] = get_OC_CONO2()/parameters[order_params['alpha']]


    data['OM_aCH_sans_C'] = get_OM_aCH_sans_C()
    data['OM_aCOH_sans_C'] = get_OM_aCOH_sans_C()
    data['OM_COOH_sans_C'] = get_OM_COOH_sans_C()
    data['OM_aNH2_sans_C'] = get_OM_aNH2_sans_C()
    data['OM_COO_sans_C'] = get_OM_COO_sans_C()
    data['OM_CO_sans_C'] = get_OM_CO_sans_C()
    data['OM_CONO2_sans_C'] = get_OM_CONO2_sans_C()


    data['OM_aCH'] = get_OM_aCH()
    data['OM_aCOH'] = get_OM_aCOH()
    data['OM_COOH'] = get_OM_COOH()
    data['OM_aNH2'] = get_OM_aNH2() 
    data['OM_COO'] = get_OM_COO() 
    data['OM_CO'] = get_OM_CO() 
    data['OM_CONO2'] = get_OM_CONO2()


    data['O'] = get_O() 
    data['C'] = get_C() 
    data['H'] = get_H()
    data['N'] = get_N() 
    data['OM'] = get_OM()
    data['OM_sans_C'] = get_OM_sans_C()

    data['OC'] = get_OC()
    data['OC_alpha'] = get_OC()/parameters[order_params['alpha']]
    data['ratio_sans_alpha'] = data['OM']/data['OC']
    data['ratio'] = 1 + data['OM_sans_C']/(data['OC']) *parameters[order_params['alpha']]
    data['sigmas_kappa'] = get_r()


    if 'kappa' in list(order_params.keys()):
        data['sigmas_kappa'] = get_sigmas(parameters[order_params['kappa']], Y,model_spe['kappa_intercept'])
    data['log_lik'] = get_likelihood()
    data['r'] = get_r()


    
    
    return(data)

