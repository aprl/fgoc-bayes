
from pathlib import Path
import sys
import os
import subprocess




self_path = Path().resolve()
sys.path.append(f'{self_path.parents[1]}/')


from main.models.paths_results import get_results_folder, path_metropolis, path_laplace_LBFGS, create_folder
from main.visualizing.utils import LaTeX_template

from main.visualizing.comparing_methods import plot_comparing_all, generate_pdf_comparing
from main.visualizing.visualizing_metropolis import plot_metropolis, generate_pdf_metropolis

_DEFAULT_BINS = [10, 10, 10, 10, 60, 60, 60, 10]


def visualising_all(run, bins=_DEFAULT_BINS):
    """ Plot all plots from metropolis and comparison between laplace and metropolis
    generate a pdf called "report.pdf" with all the figures.

    param run:      name of run
    param bins:     vector of number of bins to plot disitributions
    clusters:       list of clusters


    """
    path = get_results_folder('run702')
    subfolders = [f.name for f in os.scandir(path) if f.is_dir() ]  
    clusters = [int(f) for f in subfolders if len(f) == 2 and f.isdigit()]
    if clusters:
        cluster = clusters[0]
    else:
        cluster = None

    results_computed = [None] * 2
    results_computed[0] = int(len(os.listdir(path_metropolis(run, cluster)['chain1']))>1)
    results_computed[1] = int(len(os.listdir(path_laplace_LBFGS(run, cluster)))>1)


    folder_to_plot = os.path.join(get_results_folder(run), 'visualising')
    create_folder(folder_to_plot)
    if sum(results_computed)>0:
        plot_comparing_all(run, clusters, chain = 'chain1', bins=bins)

    if results_computed[0]:
        params = plot_metropolis(run, clusters=clusters, chain = 'chain1', bins= bins)


    outfile = os.path.join(folder_to_plot, 'report.tex')

    with open(outfile, 'w') as f:

        template = LaTeX_template(f)

        template.preamble()

        template.begin()

        if sum(results_computed)>0:
            template.write_section('Posterior distribution')
            template.includegraphics(os.path.join(folder_to_plot, 'comparison_methods.png'), options=r'width=\textwidth')


        if results_computed[0]:
            generate_pdf_metropolis(run, template, clusters = clusters)

        template.end()

        ## -----------------------------------------------------------------------------

        ## compile

    t = subprocess.call(['pdflatex', outfile])
    if Path(os.path.join(folder_to_plot,'report.pdf' )).is_file():
        os.remove(os.path.join(folder_to_plot,'report.pdf' ))
    os.rename("report.pdf", os.path.join(folder_to_plot,'report.pdf' ))
    return params