
# coding: utf-8

# In[84]:


from pathlib import Path
import sys
import json
import os
import glob
import subprocess

import pandas as pd
from pandas.api.types import CategoricalDtype
import numpy as np 
import re

import plotnine
from plotnine import *


from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.patches as patches
import seaborn as sns
from matplotlib import cm
from matplotlib.colors import ListedColormap, LinearSegmentedColormap

path = Path().resolve()
sys.path.append(f'{path.parents[1]}/')


from main.models.paths_results import path_metropolis
from main.models.utils import read_json

from main.visualizing.utils import get_results_metropolis, get_model_spe, get_constituents, _handle_ticks, plot_priors, titles_for_plots, LaTeX_template
from main.models.paths_results import create_folder, get_results_folder, path_laplace_LBFGS
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
plt.rcParams["figure.figsize"] = [20,9]
plt.rcParams["figure.titlesize"] = 32

plt.rcParams["axes.titlesize"] = 28
plt.rcParams["axes.labelsize"]= 28
plt.rcParams["xtick.labelsize"] = 30
plt.rcParams["ytick.labelsize"] = 30
plt.style.use('seaborn-deep')

import pandas as pd
from datetime import datetime
import warnings
import numpy as np
import scipy
from scipy import stats
from scipy import odr
import sklearn
from sklearn import linear_model
warnings.filterwarnings("ignore")
from sklearn.metrics import mean_squared_error
import math
import random
import sys
from matplotlib.lines import Line2D

theme_set(
    theme(
        figure_size = (16, 12),
        text = element_text(
            size = 8,
            color = 'black',
            family = 'Arial'
        ),
        panel_background=element_rect(fill='white'),
        plot_title = element_text(
            color = 'black',
            family = 'Arial',
            weight = 'bold',
            size = 26
        ),
        axis_title = element_text(
            family = 'Arial',
            size = 28
        ),
        axis_text = element_text(
            size=24
        ),
        legend_text = element_text(
        size=18
        ),
        legend_title =element_text(
        size=24),
        panel_grid=element_blank(),
        panel_border=element_blank())
    
)




_DEFAULT_BINS = [50, 50, 50,50, 2, 2, 4, 50]

def get_folder_to_plot(run, cluster = None):
    """Get to folder where to plot all the graphs for metropolis   
    :param run:             name of run
    :param cluster:         cluster of interest
    :return                 path to folder where to plot
    """
    plot_folder = get_results_folder(run)
    if cluster is not None:
        plot_folder = os.path.join(plot_folder, str(cluster).zfill(2))
    plot_folder = os.path.join(plot_folder, 'visualising')
    plot_folder = os.path.join(plot_folder, 'metropolis')
    create_folder(plot_folder)
    return plot_folder



def f(B, x):
    """linear function

    :param B:    slope
    :param x:    data x

    :return: B*x
    """
    return B[0]*x


def plot_distributions(params_to_be_plotted, bins, run, chain, model_spe, folder_to_plot, cluster = None):
    """Central function to plot all priors on metropolis plot

    :param params_to_be_plotted:    all the parameters to be plotted
    :param bins:                    list of bins for metropolis plot
    :param run:                     name of run
    :param chain:                   chain of interest
    :param model_spe:               dict containing model specificities
    :params folder_to_plot          path to folder where to plot
    :param cluster:                 cluster of interest


    :return: -
    """
    fig, axes = plt.subplots(nrows=2, ncols=4)
    axes_ = axes.reshape(-1)
    df = get_results_metropolis(run,  chain, cluster)
    for n in range(0, len(params_to_be_plotted)):
        f = 0
        range_ = None
        if params_to_be_plotted[n] in ['ncomp_aCH','ncomp_aCOH','ncomp_COOH']:
            f = 1
            range_ = (0,30)
        axes_[n].hist(df[params_to_be_plotted[n]]+f,bins = bins[n],alpha=0.8, density = True, range =range_)
        axes_[n].set_title(titles_for_plots[params_to_be_plotted[n]], y =1.01)

    _handle_ticks(axes_, params_to_be_plotted)
    plot_priors(run, params_to_be_plotted, axes_, model_spe)
    if 'kappa' not in params_to_be_plotted:
        fig.delaxes(axes_[7])
    fig.tight_layout()
    if cluster is not None:
        t1 = fig.suptitle('Cluster: '+str(cluster), y= 1.04)
        plt.savefig(os.path.join(folder_to_plot, 'metropolis.pdf'),bbox_inches='tight', bbox_extra_artists=[t1] ) 
    else:
        plt.savefig(os.path.join(folder_to_plot, 'metropolis.pdf') ) 
    plt.close()
    return


# In[44]:


def get_best_solution(df, order_params):
    """Get most likely solution from metropolis runs

    :param df:                  dataframe containing metropolis results
    :param order_params:        order of parameters in model spe


    :return: list of parameters of the best solution in the order
             given by order_params from model_spe
    """
    temp = df.copy()
    temp = temp.sort_values('posterior', ascending = False)
    best_solu = temp.iloc[0]
    best_solu = list(best_solu[list(order_params.keys())])
    return best_solu


# In[45]:





def get_season_string(season):
    """ from season in integer returns season in string
    
    param season:       1, 2, 3 or 4

    return season:      DJF, MAM, JJA or SON
    """
    if season ==1:
        r = 'DJF'
    elif season ==2:
        r = 'MAM'
    elif season == 3:
        r = 'JJA'
    else:
        r = 'SON'
    return r


def plot_hist(results, folder_to_plot):
    """Produce and save histogram of OM/OC and O/C predictions for a fixed set of parameters

    :param results:             dataframe with different predictions from a fixed set of parameters
                                results is produced by main.Visualising.utils
    :params folder_to_plot:     path to folder where to plot

    :return: -
    """
    plt.rcParams["xtick.labelsize"] = 30

    fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(16,9))
    results = results[results['ratio']<3]
        


    axes[0].hist(results['ratio'], bins=50)
    axes[0].set_title('OM/OC', size =40)
    #axes[0,0].set_ylabel('Count')
    axes[0].set_yticks([])
    axes[0].set_xlim(0.95, 2.95)

    axes[1].hist(results['O']/results['C'], bins=50)
    axes[1].set_title('O/C', size =40)
    axes[1].set_xlim([0,1.32])
    axes[1].set_yticks([])

    fig.tight_layout()
    name = 'histo_ratios'
    #plt.savefig(os.path.join(folder_to_plot, name+'.pdf'),bbox_inches='tight', bbox_extra_artists=[t1] )
    plt.savefig(os.path.join(folder_to_plot, name+'.pdf'))
    return


# In[51]:


def plot_ratio_per_site_and_season(results, folder_to_plot):
    """Produce and save barplot of OM/OC predictions for a fixed set of parameters
    per site and season

    :param results:             dataframe with different predictions from a fixed set of parameters
                                results is produced by main.Visualising.utils
    :params folder_to_plot:     path to folder where to plot

    :return:    -
    """
    results2 = results.copy(deep = True)
    results2.loc[results2.SiteCode.isin(['PHX1A', 'PHX5A']), 'SiteCode'] = 'PHOE1'
    results2['SiteCode'] = results2['SiteCode'].apply(lambda x: x.replace('1', '').replace('X', '').replace('5', ''))
    # Determine order and create a categorical type
    sitecodes = ['BIRM', 'FRES', 'PHOE', 'PUSO', 'BLIS', 'GRSM', 'HOOV', 'MEVE', 'OKEF', 'OLYM', 'PMRF', 'ROMA', 'SAFO', 'SAMA', 'TALL', 'TRCR', 'YOSE', 'all']
    sitecodes = CategoricalDtype(categories=sitecodes, ordered=True)

    # Cast the existing categories into the new category. Due to a bug in pandas
    # we need to do this via a string.
    results2['SiteCode'] = results2['SiteCode'].astype(str).astype(sitecodes)
    per_site_and_season = results2.groupby(['season', 'SiteCode'], as_index=False).median()
    per_site_and_season['ratio'] = per_site_and_season['ratio']-1
    per_site_and_season = per_site_and_season.dropna()



    min_ = 0
    max_ = round(max(per_site_and_season['ratio']),1)+0.1
    breaks = list(np.arange(min_,max_,0.25))
    breaks_ = [round(x,1) for x in breaks]
    per_site_and_season = per_site_and_season.rename(columns={'season': 'Season'})
    plot = (ggplot(per_site_and_season, aes(x='SiteCode', y='ratio', fill='Season'))
     + geom_bar(stat='identity', position='dodge', width=0.7)
    +ylab('OM/OC')
    +xlab('')
    +theme(axis_text_x=element_text(rotation=45, hjust=1, size = 18))
   + scale_y_continuous(expand = (0,0), breaks=breaks_, labels =[x+1 for x in breaks_], limits = (0, max_)))
    #+ theme(axis.title = 22))
    name ='ratio_per_site_and_season'
    plot.save(os.path.join(folder_to_plot, name+'.pdf'))
    return



def plot_ratio_per_type_and_season(results, folder_to_plot):
    """Produce and save barplot of OM/OC predictions for a fixed set of parameters
    per site and season

    :param results:             dataframe with different predictions from a fixed set of parameters
                                results is produced by main.Visualising.utils
    :params folder_to_plot:     path to folder where to plot

    :return:    -
    """
    per_site_and_season = results.groupby(['season', 'type'], as_index=False).median()
    per_site_and_season['ratio'] = per_site_and_season['ratio']-1
    per_site_and_season = per_site_and_season.dropna()
    per_site_and_season['type'] = per_site_and_season['type'].apply(lambda x: x.capitalize())
    min_ = 0
    max_ = round(max(per_site_and_season['ratio']),1)+0.1
    breaks = list(np.arange(min_,max_,0.2))
    breaks_ = [round(x,1) for x in breaks]
    per_site_and_season = per_site_and_season.rename(columns={'season': 'Season'})

    plot = (ggplot(per_site_and_season, aes(x='type', y='ratio', fill='Season'))
     + geom_bar(stat='identity', position='dodge', width=0.7)
    +ylab('OM/OC')
    +xlab('')
    #+ggtitle(text)
   + scale_y_continuous(expand = (0,0), limits=(0, max_), breaks=breaks, labels =[x+1 for x in breaks]))
    #+ theme(axis.title = 22))

    name ='ratio_per_type_and_season'
    plot.save(os.path.join(folder_to_plot, name+'.pdf'))
    return

# In[52]:

def plot_ratio_per_cluster(results1, folder_to_plot):
    """ Plot distribution of OM/OC per cluster and save plot

    param results1:         MAP predictions
    param folder_to_plot:   folder where to save file
    """
    results1_temp = results1.copy()
    results1_temp['ratio'] = results1_temp['ratio']-1
    df1 = results1_temp.groupby('cluster')\
    .agg({'ratio':{'mean': lambda x: x.mean(), 
                    'std': lambda x: np.std(x, ddof=1)}})

    df1.reset_index(level=0, inplace=True)
    df1.columns = df1.columns.get_level_values(0)
    df1.columns = ['cluster', 'mean', 'sd']
    df1['min'] = df1['mean']-df1['sd']
    df1['max'] = df1['mean']+df1['sd']
    min_ = 0
    max_ = np.max(df1['max'])+0.1
    breaks = list(np.arange(min_,max_,0.5))

    plot = (ggplot(df1, aes(x='cluster', y = 'mean'))
    +geom_col()
    +geom_errorbar(aes(ymin='min', ymax = 'max'))
    + scale_x_continuous(breaks=list(range(10)), labels =[x+1 for x in list(range(10))])
    + scale_y_continuous(expand = (0,0), limits=(0, max_), breaks=breaks, labels =[x+1 for x in breaks])
    +xlab('Cluster')
    +ylab('OM/OC')
    +ggtitle('OM/OC per cluster'))
    plot.save(os.path.join(folder_to_plot, 'ratio_per_cluster.pdf'))
    return



def plot_O_C_per_site_and_season(results, folder_to_plot):
    """Produce and save barplot of O/C predictions for a fixed set of parameters
    per site and season

    :param results:             dataframe with different predictions from a fixed set of parameters
                                results is produced by main.Visualising.utils
    :params folder_to_plot:     path to folder where to plot

    :return:    -
    """

    per_site_and_season = results.groupby(['season', 'SiteCode'], as_index=False).median()
    plot = (ggplot(per_site_and_season, aes(x='SiteCode', y='O_C', fill='season'))
     + geom_bar(stat='identity', position='dodge', width=0.7)
    +theme(axis_text_x=element_text(rotation=45, hjust=1, size = 18))

    +ylab(r'$\frac{O}{C}$'))
    #+ theme(axis.title = 22))
    name ='O_C_per_site_and_season'
    plot.save(os.path.join(folder_to_plot, name+'.pdf'))
    return


# In[53]:


def plot_ratios_per_season(results, folder_to_plot):
    """Produce and save barplot of OM/OC and O/C predictions for a fixed set of parameters
    per season

    :param results:                 dataframe with different predictions from a fixed set of parameters
                                    results is produced by main.Visualising.utils
    :params folder_to_plot:         path to folder where to plot

    :return:    -
    """

    fig, axes = plt.subplots(nrows=1, ncols=2)
    test = results.groupby(results['season'])[['ratio']].median()
    ratio1 = results.groupby(results['season'])[['ratio']].median()['ratio'].values
    ratio1 = [x for x in ratio1 if str(x) != 'nan']

    y_pos = np.arange(len(ratio1))


    axes[0].bar(y_pos,ratio1, width =0.4, color='red')
    max_ratio = np.max(ratio1)+ 0.2
    axes[0].set_ylim([1, max_ratio])
    axes[0].set_xticks(y_pos)
    axes[0].set_xticklabels(list(results.groupby(results['season'])[['ratio']].median().index))
    axes[0].set_ylabel('Value', size=24)
    axes[0].set_title('OM/OC', size=30)



    results['O_C'] = results['O']/results['C']
    O_C1 = results.groupby(results['season'])[['O_C']].median()['O_C'].values



    y_pos = np.arange(len(O_C1))
    axes[1].bar(y_pos,O_C1, width =0.4, color='red')
    axes[1].set_ylim([0.,0.75])
    axes[1].set_xticks(y_pos)
    axes[1].set_title('O/C', size=30)



    axes[1].set_xticklabels(list(results.groupby(results['season'])[['ratio']].median().index))
    fig.tight_layout()
    name ='per_season'
    plt.savefig(os.path.join(folder_to_plot, name+'.pdf') )
    return


def plot_ratios_per_type(results, folder_to_plot):
    """Produce and save barplot of OM/OC and O/C predictions for a fixed set of parameters
    per season

    :param results:                 dataframe with different predictions from a fixed set of parameters
                                    results is produced by main.Visualising.utils
    :params folder_to_plot:         path to folder where to plot

    :return:    -
    """
    fig, axes = plt.subplots(nrows=1, ncols=2)
    test = results.groupby(results['type'])[['ratio']].median()
    ratio1 = results.groupby(results['type'])[['ratio']].median()['ratio'].values
    ratio1 = [x for x in ratio1 if str(x) != 'nan']

    y_pos = np.arange(len(ratio1))


    axes[0].bar(y_pos,ratio1, width =0.4, color='red')
    max_ratio = np.max(ratio1)+ 0.2
    axes[0].set_ylim([1, max_ratio])
    axes[0].set_xticks(y_pos)
    axes[0].set_xticklabels(list(results.groupby(results['type'])[['ratio']].median().index))
    axes[0].set_ylabel('Value', size=24)
    axes[0].set_title('OM/OC', size=30)



    results['O_C'] = results['O']/results['C']
    O_C1 = results.groupby(results['type'])[['O_C']].median()['O_C'].values



    y_pos = np.arange(len(O_C1))
    axes[1].bar(y_pos,O_C1, width =0.4, color='red')
    axes[1].set_ylim([0.,0.75])
    axes[1].set_xticks(y_pos)
    axes[1].set_title('O/C', size=30)



    axes[1].set_xticklabels(list(results.groupby(results['type_'])[['ratio']].median().index))
    fig.tight_layout()
    name ='ratios_per_type'
    plt.savefig(os.path.join(folder_to_plot, name+'.pdf') )
    return


# In[54]:


def plot_ratio_FG(results, folder_to_plot):
    """Produce and save barplot of OM/OC for a fixed set of parameters with contribution 
    from each FG

    :param results:             dataframe with different predictions from a fixed set of parameters
                                results is produced by main.Visualising.utils
    :params folder_to_plot:     path to folder where to plot

    :return:    -
    """



    temp = results.copy()
    temp['ratio_aCH'] = temp['OM_aCH_sans_C']/temp['OC']
    temp['ratio_aCOH'] = temp['OM_aCOH_sans_C']/temp['OC']
    temp['ratio_COOH'] = temp['OM_COOH_sans_C']/temp['OC']
    temp['ratio_aNH2'] = temp['OM_aNH2_sans_C']/temp['OC']
    temp['ratio_COO'] = temp['OM_COO_sans_C']/temp['OC']
    temp['ratio_CO'] = temp['OM_CO_sans_C']/temp['OC']
    temp['ratio_CONO2'] = temp['OM_CONO2_sans_C'] /temp['OC']
    name ='ratio_FG'

    temp = temp[['SiteCode','ratio_aCH','ratio_aCOH','ratio_COOH', 'ratio_aNH2', 'ratio_COO', 'ratio_CO', 'ratio_CONO2']]
    temp.loc[temp.SiteCode.isin(['PHX1A', 'PHX5A']), 'SiteCode'] = 'PHOE1'
    temp['SiteCode'] = temp['SiteCode'].apply(lambda x: x.replace('1', '').replace('X', '').replace('5', ''))

    temp = temp.melt(id_vars=['SiteCode'])
    temp = temp.groupby(['variable','SiteCode'], as_index=False).median()
    temp.columns = ['FG','SiteCode','value']
    temp['FG'] = temp['FG'].apply(lambda x: x.replace('ratio_',''))
    temp = temp.sort_values('FG')
    temp['FG']=pd.Categorical(temp['FG'],categories=['aCH','aCOH','COOH', 'aNH2', 'CO', 'COO', 'CONO2'], ordered=True)


    # Determine order and create a categorical type
    sitecodes = ['BIRM', 'FRES', 'PHOE', 'PUSO', 'BLIS', 'GRSM', 'HOOV', 'MEVE', 'OKEF', 'OLYM', 'PMRF', 'ROMA', 'SAFO', 'SAMA', 'TALL', 'TRCR', 'YOSE']
    sitecodes = CategoricalDtype(categories=sitecodes, ordered=True)

    # Cast the existing categories into the new category. Due to a bug in pandas
    # we need to do this via a string.
    temp['SiteCode'] = temp['SiteCode'].astype(str).astype(sitecodes)
    max_ = np.max(temp.groupby('SiteCode')['value'].sum())*1.05


    min_ = 0
    breaks = list(np.arange(min_,max_,0.2))
    breaks_ = [round(x,1) for x in breaks]
    labels_ = [x+1 for x in breaks_]



    plot = (ggplot(temp, aes(x='SiteCode', y='value', fill='FG'))
     + geom_col()
     + scale_fill_manual(values=['#1E90FF','#DB7093','#228B22', '#FFA500',  '#348168','#989898', '#8B0000'])
     + scale_y_continuous(breaks=breaks_, labels =labels_ ,expand = (0,0), limits =(0, max_))
     + theme(axis_text_x=element_text(rotation=45, hjust=1, size = 18))
     +xlab('')
     +ylab(r'$\frac{OM}{OC}$')
    )
    plot.save(os.path.join(folder_to_plot, name+'.pdf'))

    return


# In[55]:


def plot_OC_FG(results, folder_to_plot):
    """Produce and save barplot of OC for a fixed set of parameters with contribution 
    from each FG

    :param results:             dataframe with different predictions from a fixed set of parameters
                                results is produced by main.Visualising.utils
    :params folder_to_plot:     path to folder where to plot

    :return:    -
    """
    name ='OC_FG'

    temp = results[['SiteCode','OC_aCH','OC_aCOH','OC_COOH', 'OC_aNH2', 'OC_COO', 'OC_CO', 'OC_CONO2']]
    temp.loc[temp.SiteCode.isin(['PHX1A', 'PHX5A']), 'SiteCode'] = 'PHOE1'
    temp['SiteCode'] = temp['SiteCode'].apply(lambda x: x.replace('1', '').replace('X', '').replace('5', ''))

    temp = temp.melt(id_vars=['SiteCode'])
    temp = temp.groupby(['variable','SiteCode'], as_index=False).median()
    temp.columns = ['FG','SiteCode','value']
    temp['FG'] = temp['FG'].apply(lambda x: x.replace('OC_',''))
    temp = temp.sort_values('FG')
    temp['FG']=pd.Categorical(temp['FG'],categories=['aCH','aCOH','COOH', 'aNH2', 'CO', 'COO', 'CONO2'], ordered=True)

        
    # Determine order and create a categorical type
    sitecodes = ['BIRM', 'FRES', 'PHOE', 'PUSO', 'BLIS', 'GRSM', 'HOOV', 'MEVE', 'OKEF', 'OLYM', 'PMRF', 'ROMA', 'SAFO', 'SAMA', 'TALL', 'TRCR', 'YOSE']
    sitecodes = CategoricalDtype(categories=sitecodes, ordered=True)

    # Cast the existing categories into the new category. Due to a bug in pandas
    # we need to do this via a string.
    temp['SiteCode'] = temp['SiteCode'].astype(str).astype(sitecodes)
    max_ = np.max(temp['value'])*1.05


    plot = (ggplot(temp, aes(x='SiteCode', y='value', fill='FG'))
     + geom_col()
     + scale_fill_manual(values=['#1E90FF','#DB7093','#228B22', '#FFA500',  '#348168','#989898', '#8B0000'])
     +ylab(r'OC [$\mu g/cm^2$]')
     +theme(axis_text_x=element_text(rotation=45, hjust=1, size = 18))
     +scale_y_continuous(expand = (0,0), limits =(0, max_))
     +xlab('') )
    plot.save(os.path.join(folder_to_plot, name+'.pdf'))

    return


# In[56]:


def plot_OM_FG(results, folder_to_plot):
    """Produce and save barplot of OM for a fixed set of parameters with contribution 
    from each FG

    :param results:             dataframe with different predictions from a fixed set of parameters
                                results is produced by main.Visualising.utils
    :params folder_to_plot:     path to folder where to plot

    :return:    -
    """
    name ='OM_FG'

    temp = results[['SiteCode','OM_aCH','OM_aCOH','OM_COOH','OM_aNH2','OM_COO','OM_CO', 'OM_CONO2']].copy()
    temp.loc[temp.SiteCode.isin(['PHX1A', 'PHX5A']), 'SiteCode'] = 'PHOE1'
    temp['SiteCode'] = temp['SiteCode'].apply(lambda x: x.replace('1', '').replace('X', '').replace('5', ''))

    # Determine order and create a categorical type
    sitecodes = ['BIRM', 'FRES', 'PHOE', 'PUSO', 'BLIS', 'GRSM', 'HOOV', 'MEVE', 'OKEF', 'OLYM', 'PMRF', 'ROMA', 'SAFO', 'SAMA', 'TALL', 'TRCR', 'YOSE']
    sitecodes = CategoricalDtype(categories=sitecodes, ordered=True)

    # Cast the existing categories into the new category. Due to a bug in pandas
    # we need to do this via a string.
    temp['SiteCode'] = temp['SiteCode'].astype(str).astype(sitecodes)


    temp = temp.melt(id_vars=['SiteCode'])
    temp = temp.groupby(['variable','SiteCode'], as_index=False).median()
    temp.columns = ['FG','SiteCode','value']
    temp['FG'] = temp['FG'].apply(lambda x: x.replace('OM_',''))
    temp = temp.sort_values('FG')

    FGs = ['aCH','aCOH','COOH', 'aNH2', 'CO', 'COO', 'CONO2']
    FGs = CategoricalDtype(categories=FGs, ordered=True)
    temp['FG'] = temp['FG'].astype(str).astype(FGs)


    #temp['FG']=pd.Categorical(temp['FG'],categories=['aCH','aCOH','COOH', 'aNH2', 'CO', 'COO', 'CONO2'], ordered=True)
    max_ = np.max(temp.groupby('SiteCode')['value'].sum())



    plot = (ggplot(temp, aes(x='SiteCode', y='value', fill='FG'))
     + geom_col()
     + scale_fill_manual(values=['#1E90FF','#DB7093','#228B22', '#FFA500',  '#348168','#989898', '#8B0000'])
     +ylab(r'OM [$\mu g/cm^2$]')
     +theme(axis_text_x=element_text(rotation=45, hjust=1, size = 18))
     +scale_y_continuous(expand = (0,0), limits =(0, 1.05*max_))
     +xlab('')
    )
    plot.save(os.path.join(folder_to_plot, name+'.pdf'))

    return


# In[57]:


def plot_scatter(results, folder_to_plot):
    """Produce and save scatter plot of predictions of OC/alpha and TOR OC

    :param results:             dataframe with different predictions from a fixed set of parameters
                                results is produced by main.Visualising.utils
    :params folder_to_plot:     path to folder where to plot

    :return:    -
    """
    y1 = results['OC']
    x = results['OCf:Value']

    
    
    import matplotlib.pyplot as plt
    from matplotlib.lines import Line2D
    plt.rcParams["figure.figsize"] = [16,9]
    fig, axes=plt.subplots(nrows=1, ncols=1)
    
    cor1_=scipy.stats.pearsonr(x, y1)
    R1=cor1_[0]
    R1 = int((R1 * 100) + 0.5) / 100.0

    linear = odr.Model(f)  
    mydata = odr.Data(x, y1)
    myodr = odr.ODR(mydata, linear, beta0=[1])
    myoutput = myodr.run()
    slope1=myoutput.beta
    slope1 = int((slope1 * 100) + 0.5) / 100.0

    l = Line2D([-100,100],[-100,100],c='r')

    axes.scatter(x,y1, color='red')
    axes.set_ylabel(r'$OC_{pred} [\mu g/cm^{2}]$',size=20)
    lims = [
        np.min([axes.get_xlim(), axes.get_ylim()]),  # min of both axes
        np.max([axes.get_xlim(), axes.get_ylim()]),  # max of both axes
    ]
    axes.plot(lims, lims, 'k-', alpha=0.75, zorder=0)
    axes.set_xlim(lims)
    axes.set_ylim(lims)
    max_ = np.max(x)
    min_ = np.min(x)


    axes.text(0.75*max_,0.15*max_, 'Slope='+str(slope1),size=20)
    axes.text(0.75*max_,0.05*max_,' R='+str(R1),size=20)
    axes.set_aspect('equal')
    t1 = axes.set_xlabel( r'TOR OC $[\mu g/cm^{2}]$', size = 20)

    plt.savefig(os.path.join(folder_to_plot,'scatter.pdf'),bbox_inches='tight', bbox_extra_artists=[t1] )

    return

def plot_scatter_alpha(results, folder_to_plot):
    """Produce and save scatter plot of predictions of OC/alpha and TOR OC

    :param results:             dataframe with different predictions from a fixed set of parameters
                                results is produced by main.Visualising.utils
    :params folder_to_plot:     path to folder where to plot

    :return:    -
    """
    y1 = results['OC_alpha']
    x = results['OCf:Value']

    
    
    import matplotlib.pyplot as plt
    from matplotlib.lines import Line2D
    plt.rcParams["figure.figsize"] = [16,9]
    fig, axes=plt.subplots(nrows=1, ncols=1)
    
    cor1_=scipy.stats.pearsonr(x, y1)
    R1=cor1_[0]
    R1 = int((R1 * 100) + 0.5) / 100.0

    linear = odr.Model(f)  
    mydata = odr.Data(x, y1)
    myodr = odr.ODR(mydata, linear, beta0=[1])
    myoutput = myodr.run()
    slope1=myoutput.beta
    slope1 = int((slope1 * 100) + 0.5) / 100.0

    l = Line2D([-100,100],[-100,100],c='r')

    axes.scatter(x,y1, color='red')
    axes.set_ylabel(r'$\frac{OC_{pred}}{\alpha} [\mu g/cm^{2}]$',size=20)
    lims = [
        np.min([axes.get_xlim(), axes.get_ylim()]),  # min of both axes
        np.max([axes.get_xlim(), axes.get_ylim()]),  # max of both axes
    ]
    axes.plot(lims, lims, 'k-', alpha=0.75, zorder=0)
    axes.set_xlim(lims)
    axes.set_ylim(lims)
    max_ = np.max(x)
    min_ = np.min(x)


    axes.text(0.75*max_,0.15*max_, 'Slope='+str(slope1),size=20)
    axes.text(0.75*max_,0.05*max_,' R='+str(R1),size=20)
    axes.set_aspect('equal')
    t1 = axes.set_xlabel( r'TOR OC $[\mu g/cm^{2}]$', size = 20)

    plt.savefig(os.path.join(folder_to_plot,'scatter_alpha.pdf'),bbox_inches='tight', bbox_extra_artists=[t1] )

    return

def get_dim_plot(x):
    """ from index of plot return row and column index

    param x:        index of plot
    
    return row, col indexes
    """
    row = math.ceil(x**0.5)
    col = row
    for i in range(x-1, 0, -1):
        if i*row >= x:
            col = i
        else:
            break
    return row, col

def plot_scatter_per_site(results1, folder_to_plot):
    """Produce and save scatter plot of predictions of OC and TOR OC with each site as a subplot

    :param results1:             dataframe with different predictions from a fixed set of parameters
                                results is produced by main.Visualising.utils
    :params folder_to_plot:     path to folder where to plot

    :return:    -
    """
    sites = list(set(results1['SiteCode']))
    import matplotlib.pyplot as plt
    from matplotlib.lines import Line2D
    plt.rcParams["figure.figsize"] = [16,16]
    row, col = get_dim_plot(len(sites))
    fig, axes=plt.subplots(nrows=row, ncols=col)
    axes_ = axes.reshape(-1)
    fig.delaxes(axes_[8])

    for n in range(0, len(sites)):
        site = sites[n]
        results = results1[results1['SiteCode']==site]
        if len(results)>1:
            y1 = results['OC']
            x = results['OCf:Value']
            cor1_=scipy.stats.pearsonr(x, y1)
            R1=cor1_[0]
            R1 = int((R1 * 100) + 0.5) / 100.0

            linear = odr.Model(f)  
            mydata = odr.Data(x, y1)
            myodr = odr.ODR(mydata, linear, beta0=[1])
            myoutput = myodr.run()
            slope1=myoutput.beta
            slope1 = int((slope1 * 100) + 0.5) / 100.0

            l = Line2D([-100,100],[-100,100],c='r')

            axes_[n].scatter(x,y1, color='red')
            lims = [
                np.min([axes_[n].get_xlim(), axes_[n].get_ylim()]),  # min of both axes
                np.max([axes_[n].get_xlim(), axes_[n].get_ylim()]),  # max of both axes
            ]
            axes_[n].plot(lims, lims, 'k-', alpha=0.75, zorder=0)
            axes_[n].set_xlim([0,lims[1]])
            axes_[n].set_ylim([0,lims[1]])

            max_ = np.max(x)
            axes_[n].set_title(site)
            axes_[n].text(0.05*max_,0.93*max_, 'Slope='+str(slope1),size=18)
            axes_[n].text(0.05*max_,0.85*max_,' R='+str(R1),size=18)
            axes_[n].set_aspect('equal')
    t1 = fig.text(0.45,-0.02, r'TOR OC $[\mu g/cm^{2}]$', size =28)
    t2 = fig.text(-0.04,0.55, r'$\frac{OC_{pred}}{\alpha} [\mu g/cm^{2}]$',rotation=90, size =28)
    fig.tight_layout()
    plt.savefig(os.path.join(folder_to_plot,'scatter_per_site.pdf'),bbox_inches='tight', bbox_extra_artists=[t1, t2] )
    return 

def plot_scatter_per_season(results1, folder_to_plot):
    """Produce and save scatter plot of predictions of OC and TOR OC with each season as a subplot

    :param results1:             dataframe with different predictions from a fixed set of parameters
                                results is produced by main.Visualising.utils
    :params folder_to_plot:     path to folder where to plot

    :return:    -
    """
    import matplotlib.pyplot as plt
    from matplotlib.lines import Line2D
    plt.rcParams["figure.figsize"] = [16,16]
    fig, axes=plt.subplots(nrows=2, ncols=2)

    
    axes_ = axes.reshape(-1)
    seasons = list(set(results1['season']))
    cmap = cm.get_cmap('tab10', 10)
    for n in range(0, len(seasons)):
        season = seasons[n]
        results = results1[results1['season']==season]

        y1 = results['OC']
        x = results['OCf:Value']
        colors_ = results['cluster']
        cor1_=scipy.stats.pearsonr(x, y1)
        R1=cor1_[0]
        R1 = int((R1 * 100) + 0.5) / 100.0

        linear = odr.Model(f)  
        mydata = odr.Data(x, y1)
        myodr = odr.ODR(mydata, linear, beta0=[1])
        myoutput = myodr.run()
        slope1=myoutput.beta
        slope1 = int((slope1 * 100) + 0.5) / 100.0

        l = Line2D([-100,100],[-100,100],c='r')
        if season =='MAM':
            scat = axes_[n].scatter(x,y1, c=colors_.values, s= 25, cmap = cmap, alpha = .7)
        else:
            axes_[n].scatter(x,y1, c=colors_.values, s= 25, cmap = cmap, alpha = .7)
        lims = [
            np.min([axes_[n].get_xlim(), axes_[n].get_ylim()]),  # min of both axes
            np.max([axes_[n].get_xlim(), axes_[n].get_ylim()]),  # max of both axes
        ]
        axes_[n].plot(lims, lims, 'k-', alpha=0.75, zorder=0)
        axes_[n].set_xlim([0,lims[1]])
        axes_[n].set_ylim([0,lims[1]])


        max_ = np.max(x)
        axes_[n].set_title(season)
        axes_[n].text(0.05*max_,0.93*max_, 'slope = '+str(slope1),size=22)
        axes_[n].text(0.05*max_,0.85*max_,'r = '+str(R1),size=22)
        axes_[n].set_aspect('equal')
    
    fig.subplots_adjust(right=0.8)
    # put colorbar at desire position
    cbar_ax = fig.add_axes([1.05, 0.15, 0.05, 0.7])
    fig.colorbar(scat, cax=cbar_ax)
    cbar_ax.set_title('Cluster', y= 1.03)
    t1 = fig.text(0.42,-0.02, r'TOR OC $[\mu g/cm^{2}]$', size =28)
    t2 = fig.text(-0.04,0.55, r'$OC_{pred} [\mu g/cm^{2}]$',rotation=90, size =28)
    fig.tight_layout()
    plt.savefig(os.path.join(folder_to_plot,'scatter_per_season.pdf'),bbox_inches='tight', bbox_extra_artists=[t1, t2] )
    return

def plot_scatter_per_season_alpha(results1, folder_to_plot):
    """Produce and save scatter plot of predictions of OC/alpha and TOR OC with each season as a subplot

    :param results1:             dataframe with different predictions from a fixed set of parameters
                                results is produced by main.Visualising.utils
    :params folder_to_plot:     path to folder where to plot

    :return:    -
    """
    import matplotlib.pyplot as plt
    from matplotlib.lines import Line2D
    plt.rcParams["figure.figsize"] = [16,16]
    fig, axes=plt.subplots(nrows=2, ncols=2)

    
    axes_ = axes.reshape(-1)
    seasons = list(set(results1['season']))
    cmap = cm.get_cmap('tab10', 10)
    for n in range(0, len(seasons)):
        season = seasons[n]
        results = results1[results1['season']==season]

        y1 = results['OC_alpha']
        x = results['OCf:Value']
        colors_ = results['cluster']
        cor1_=scipy.stats.pearsonr(x, y1)
        R1=cor1_[0]
        R1 = int((R1 * 100) + 0.5) / 100.0

        linear = odr.Model(f)  
        mydata = odr.Data(x, y1)
        myodr = odr.ODR(mydata, linear, beta0=[1])
        myoutput = myodr.run()
        slope1=myoutput.beta
        slope1 = int((slope1 * 100) + 0.5) / 100.0

        l = Line2D([-100,100],[-100,100],c='r')
        if season =='MAM':
            scat = axes_[n].scatter(x,y1, c=colors_.values, s= 25, cmap = cmap, alpha = .7)
        else:
            axes_[n].scatter(x,y1, c=colors_.values, s= 25, cmap = cmap, alpha = .7)
        lims = [
            np.min([axes_[n].get_xlim(), axes_[n].get_ylim()]),  # min of both axes
            np.max([axes_[n].get_xlim(), axes_[n].get_ylim()]),  # max of both axes
        ]
        axes_[n].plot(lims, lims, 'k-', alpha=0.75, zorder=0)
        axes_[n].set_xlim([0,lims[1]])
        axes_[n].set_ylim([0,lims[1]])


        max_ = np.max(x)
        axes_[n].set_title(season)
        axes_[n].text(0.05*max_,0.93*max_, 'slope = '+str(slope1),size=22)
        axes_[n].text(0.05*max_,0.85*max_,'r = '+str(R1),size=22)
        axes_[n].set_aspect('equal')
    
    fig.subplots_adjust(right=0.8)
    # put colorbar at desire position
    cbar_ax = fig.add_axes([1.05, 0.15, 0.05, 0.7])
    cbar_ax.set_title('Cluster', y= 1.03)
    fig.colorbar(scat, cax=cbar_ax)
    t1 = fig.text(0.42,-0.02, r'TOR OC $[\mu g/cm^{2}]$', size =28)
    t2 = fig.text(-0.04,0.55, r'$\frac{OC_{pred}}{\alpha} [\mu g/cm^{2}]$',rotation=90, size =28)
    fig.tight_layout()
    plt.savefig(os.path.join(folder_to_plot,'scatter_per_season_alpha.pdf'),bbox_inches='tight', bbox_extra_artists=[t1, t2] )
    return

def plot_scatter_per_type(results1, folder_to_plot= None):
    """Produce and save scatter plot of predictions of OC and TOR OC with urban and rural as subplots

    :param results1:             dataframe with different predictions from a fixed set of parameters
                                results is produced by main.Visualising.utils
    :params folder_to_plot:     path to folder where to plot

    :return:    -
    """
    import matplotlib.pyplot as plt
    import matplotlib
    from matplotlib.lines import Line2D
    fig, axes = plt.subplots(nrows=1,ncols = 3, figsize=(18,8), 
                  gridspec_kw={"width_ratios":[1, 1, 0.05]})    
    axes_ = axes.reshape(-1)
    types = list(set(results1['type']))
    for n in range(0, len(types)):
        type_ = types[n]
        results = results1[results1['type']==type_]

        y1 = results['OC']
        x = results['OCf:Value']
        cor1_=scipy.stats.pearsonr(x, y1)
        colors_ = results['cluster']

        R1=cor1_[0]
        R1 = int((R1 * 100) + 0.5) / 100.0

        cmap = cm.get_cmap('tab10', 10)

        linear = odr.Model(f)  
        mydata = odr.Data(x, y1)
        myodr = odr.ODR(mydata, linear, beta0=[1])
        myoutput = myodr.run()
        slope1=myoutput.beta
        slope1 = int((slope1 * 100) + 0.5) / 100.0

        l = Line2D([-100,100],[-100,100],c='r')
        scat = axes_[n].scatter(x,y1, c=colors_.values, s= 18, cmap = cmap, alpha = .7)

        lims = [
            np.min([axes_[n].get_xlim(), axes_[n].get_ylim()]),  # min of both axes
            np.max([axes_[n].get_xlim(), axes_[n].get_ylim()]),  # max of both axes
        ]
        axes_[n].plot(lims, lims, 'k-', alpha=0.75, zorder=0)
        axes_[n].set_xlim([0,lims[1]])
        axes_[n].set_ylim([0,lims[1]])



        max_ = np.max(x)
        axes_[n].set_title(type_.capitalize(), size = 32)
        axes_[n].text(0.05*max_,0.93*max_, 'slope = '+str(slope1),size=24)
        axes_[n].text(0.05*max_,0.85*max_,'r = '+str(R1),size=24)
        axes_[n].set_aspect('equal')
    axes_[2].yaxis.set_ticks_position('left')
    axes_[2].set_title('Cluster', y= 1.03)
    fig.colorbar(scat, cax=axes_[2])
    t1 = fig.text(0.37,-0.03, r'TOR OC $[\mu g/cm^{2}]$', size =28)
    t2 = fig.text(-0.04,0.65, r'$OC_{pred} [\mu g/cm^{2}]$',rotation=90, size =28)
    fig.tight_layout()
    plt.savefig(os.path.join(folder_to_plot,'scatter_per_type.pdf'),bbox_inches='tight', bbox_extra_artists=[t1, t2] )
    return

def plot_scatter_per_type_alpha(results1, folder_to_plot= None):
    """Produce and save scatter plot of predictions of OC/alpha and TOR OC with urban and rural as subplots

    :param results1:             dataframe with different predictions from a fixed set of parameters
                                results is produced by main.Visualising.utils
    :params folder_to_plot:     path to folder where to plot

    :return:    -
    """
    from matplotlib import cm
    from matplotlib.colors import ListedColormap, LinearSegmentedColormap
    import matplotlib.pyplot as plt
    import matplotlib
    from matplotlib.lines import Line2D
    fig, axes = plt.subplots(nrows=1,ncols = 3, figsize=(18,8), 
                  gridspec_kw={"width_ratios":[1, 1, 0.05]})    
    axes_ = axes.reshape(-1)
    types = list(set(results1['type']))
    for n in range(0, len(types)):
        type_ = types[n]
        results = results1[results1['type']==type_]

        y1 = results['OC_alpha']
        x = results['OCf:Value']
        cor1_=scipy.stats.pearsonr(x, y1)
        colors_ = results['cluster']

        R1=cor1_[0]
        R1 = int((R1 * 100) + 0.5) / 100.0

        cmap = cm.get_cmap('tab10', 10)

        linear = odr.Model(f)  
        mydata = odr.Data(x, y1)
        myodr = odr.ODR(mydata, linear, beta0=[1])
        myoutput = myodr.run()
        slope1=myoutput.beta
        slope1 = int((slope1 * 100) + 0.5) / 100.0

        l = Line2D([-100,100],[-100,100],c='r')
        scat = axes_[n].scatter(x,y1, c=colors_.values, s= 18, cmap = cmap, alpha = .7)

        lims = [
            np.min([axes_[n].get_xlim(), axes_[n].get_ylim()]),  # min of both axes
            np.max([axes_[n].get_xlim(), axes_[n].get_ylim()]),  # max of both axes
        ]
        axes_[n].plot(lims, lims, 'k-', alpha=0.75, zorder=0)
        axes_[n].set_xlim([0,lims[1]])
        axes_[n].set_ylim([0,lims[1]])



        max_ = np.max(x)
        axes_[n].set_title(type_.capitalize(), size = 32)
        axes_[n].text(0.05*max_,0.93*max_, 'slope = '+str(slope1),size=24)
        axes_[n].text(0.05*max_,0.85*max_,'r = '+str(R1),size=24)
        axes_[n].set_aspect('equal')
    axes_[2].yaxis.set_ticks_position('left')
    axes_[2].set_title('Cluster', y= 1.03)
    fig.colorbar(scat, cax=axes_[2])
    t1 = fig.text(0.37,-0.03, r'TOR OC $[\mu g/cm^{2}]$', size =28)
    t2 = fig.text(-0.04,0.65, r'$\frac{OC_{pred}}{\alpha} [\mu g/cm^{2}]$',rotation=90, size =28)
    fig.tight_layout()
    plt.savefig(os.path.join(folder_to_plot,'scatter_per_type_alpha.pdf'),bbox_inches='tight', bbox_extra_artists=[t1, t2] )
    return    


def plot_scatter_per_cluster(results1, folder_to_plot):
    """Produce and save scatter plot of predictions of OC and TOR OC with each cluster as a subplot

    :param results1:             dataframe with different predictions from a fixed set of parameters
                                results is produced by main.Visualising.utils
    :params folder_to_plot:     path to folder where to plot

    :return:    -
    """
    import matplotlib.pyplot as plt
    from matplotlib.lines import Line2D
    plt.rcParams["figure.figsize"] = [16,16]
    clusters = list(set(results1['cluster']))
    row, col = get_dim_plot(len(clusters))
    fig, axes=plt.subplots(nrows=col, ncols=row)
    if col>1 or row>1:
        axes_ = axes.reshape(-1)
    else:
        axes_ = [axes]

    for n in range(0, len(clusters)):
        cluster = clusters[n]
        results = results1[results1['cluster']==cluster]

        y1 = results['OC']
        x = results['OCf:Value']
        cor1_=scipy.stats.pearsonr(x, y1)
        R1=cor1_[0]
        R1 = int((R1 * 100) + 0.5) / 100.0

        linear = odr.Model(f)  
        mydata = odr.Data(x, y1)
        myodr = odr.ODR(mydata, linear, beta0=[1])
        myoutput = myodr.run()
        slope1=myoutput.beta
        slope1 = int((slope1 * 100) + 0.5) / 100.0

        l = Line2D([-100,100],[-100,100],c='r')

        axes_[n].scatter(x,y1, color='red')
        lims = [
            np.min([axes_[n].get_xlim(), axes_[n].get_ylim()]),  # min of both axes
            np.max([axes_[n].get_xlim(), axes_[n].get_ylim()]),  # max of both axes
        ]
        lims[0] = 0
        axes_[n].plot(lims, lims, 'k-', alpha=0.75, zorder=0)
        axes_[n].set_xlim([0,lims[1]])
        axes_[n].set_ylim([0,lims[1]])

        max_ = lims[1]
        axes_[n].set_title('Cluster: '+str(cluster))
        axes_[n].text(0.55*max_,0.15*max_, 'Slope='+str(slope1),size=22)
        axes_[n].text(0.55*max_,0.05*max_,' R='+str(R1),size=22)
        axes_[n].set_aspect('equal')
    t1 = fig.text(0.45,-0.02, r'TOR OC $[\mu g/cm^{2}]$', size =28)
    t2 = fig.text(-0.04,0.55, r'$OC_{pred} [\mu g/cm^{2}]$',rotation=90, size =28)
    fig.tight_layout()
    plt.savefig(os.path.join(folder_to_plot,'scatter_per_cluster.pdf'),bbox_inches='tight', bbox_extra_artists=[t1, t2] )
    return  



# In[59]:


def get_min_max(results):
    """Get min max for SNR_errors plots to have the same axis limits for all plots

    :param results:      dataframe with different predictions from a fixed set of parameters
                         results is produced by main.Visualising.utils

    :return:   min_x, max_x, min_y, max_y
    """
    min_y = np.min((results['OC']-results['OCf:Value'])/results['OCf:Unc'])
    max_y = np.max((results['OC']-results['OCf:Value'])/results['OCf:Unc'])
    
    min_y = min_y-(max_y-min_y)*0.1
    max_y = max_y+(max_y-min_y)*0.1

    min_x = np.min(results['OCf:Value']/results['OCf:Unc'])
    max_x = np.max(results['OCf:Value']/results['OCf:Unc'])
    
    min_x = min_x-(max_x-min_x)*0.1
    max_x = max_x+(max_x-min_x)*0.1
    return (min_x, max_x, min_y, max_y)


# In[60]:


def plot_SNR_errors(results, folder_to_plot):
    """Produce and save plot SNR_errors with SNR on x axis and y-y_pred/sigma on y axis

    :param results:             dataframe with different predictions from a fixed set of parameters
                                results is produced by main.Visualising.utils
    :params folder_to_plot:     path to folder where to plot

    :return:   -
    """
    plt.rcParams["figure.figsize"] = [16,9]

    y1 = (results['OC']-results['OCf:Value'])/results['OCf:Unc']
    x1 = results['OCf:Value']/results['OCf:Unc']



    fig, axes=plt.subplots(nrows=1, ncols=2)
    axes[0].scatter(x1, y1)



    min_x, max_x, min_y, max_y = get_min_max(results)

    axes[0].set_xlim([min_x, max_x])
    axes[0].set_ylim([min_y, max_y])
    axes[0].axhline(0, color='red')
    axes[0].axhline(-3, color='red', linestyle = '--')
    axes[0].axhline(3, color='red', linestyle = '--')
    axes[0].axvline(3, color='red')
    axes[0].axvline(10, color='red')
    axes[0].set_ylabel(r'$\frac{\hat{OC}-OC}{\sigma}$', size = 28)
    t1 = axes[0].set_xlabel(r'$SNR$', size = 28)

    
    
    if 'sigmas_kappa' in list(results.columns): 
        y2 = (results['OC']-results['OCf:Value'])/results['sigmas_kappa']
        x2 = results['OCf:Value']/results['sigmas_kappa']
        axes[1].scatter(x2, y2)
        #axes[1].set_xlim([min_x, max_x])
        #axes[1].set_ylim([-max_y, max_y])
        axes[1].axhline(0, color='red')
        axes[1].axhline(-3, color='red', linestyle = '--')
        axes[1].axhline(3, color='red', linestyle = '--')
        axes[1].axvline(3, color='red')
        axes[1].axvline(10, color='red')
        axes[1].set_title(r'$\sigma$ ($\kappa$)', size =32)
        t1 = fig.text(0.5,-0.02, 'SNR', size =28)


    fig.tight_layout()
    plt.savefig(os.path.join(folder_to_plot,'SNR_errors.pdf'),bbox_inches='tight', bbox_extra_artists=[t1] )
    return

def plot_log_lik(results, folder_to_plot):
    """Produce and save plot SNR_errors with SNR on x axis and y-y_pred/sigma on y axis

    :param results:             dataframe with different predictions from a fixed set of parameters
                                results is produced by main.Visualising.utils
    :params folder_to_plot:     path to folder where to plot

    :return:   -
    """
    plt.rcParams["figure.figsize"] = [16,16]
    x1 = results['r']
    y1 = results['log_lik']
    z = results['OC']
    cm = plt.cm.get_cmap('Blues')
    fig, axes=plt.subplots(nrows=1, ncols=1)
    sc = axes.scatter(x1, y1, c=z, cmap=cm)
    fig.colorbar(sc)




    axes.set_ylabel(r'$Log likelihood$', size = 28)
    t1 = axes.set_xlabel(r'$r$', size = 28)

    axes.set_title(r'log-likelihood per sample', size =32)
    
    

    fig.tight_layout()
    plt.savefig(os.path.join(folder_to_plot,'loglik_r.pdf'),bbox_inches='tight', bbox_extra_artists=[t1] )
    return

def plot_SNR_errors_per_site(results, folder_to_plot):
    """Produce and save plot SNR_errors per site with SNR on x axis and y-y_pred/sigma on y axis

    :param results:             dataframe with different predictions from a fixed set of parameters
                                results is produced by main.Visualising.utils
    :params folder_to_plot:     path to folder where to plot

    :return:   -
    """
    list_of_sites = sorted(list(set(results['SiteCode'])))
    row, col = get_dim_plot(len(list_of_sites))

    fig, axes=plt.subplots(nrows=row, ncols=col)
    plt.rcParams["figure.figsize"] = [20,20]

    axes_ = axes.reshape(-1)
    min_x, max_x, min_y, max_y = get_min_max(results)


    for n in range(0, len(list_of_sites)):
        temp = results[results['SiteCode']==list_of_sites[n]]
        y1 = (temp['OC']-temp['OCf:Value'])/temp['OCf:Unc']
        x1 = temp['OCf:Value']/temp['OCf:Unc']

        axes_[n].scatter(x1, y1)
        #min_x, max_x, max_y = get_max_lims(axes)
        axes_[n].scatter(x1, y1)

        axes_[n].set_xlim([min_x, max_x])
        axes_[n].set_ylim([-max_y, max_y])
        axes_[n].axhline(0, color='red')
        axes_[n].axhline(-3, color='red', linestyle = '--')
        axes_[n].axhline(3, color='red', linestyle = '--')
        axes_[n].axvline(3, color='red')
        axes_[n].axvline(10, color='red')
        axes_[n].set_title(list_of_sites[n], size =32)

        if n<6:
            axes_[n].set_xticks([])
        if (n)%3!=0:
            axes_[n].set_yticks([])
    fig.delaxes(axes_[8])

    fig.tight_layout()
    plt.savefig(os.path.join(folder_to_plot,'SNR_errors_per_site.pdf'))
    plt.rcParams["figure.figsize"] = [16,9]

    return

def plot_SNR_errors_per_season(results, folder_to_plot):
    """Produce and save plot SNR_errors per season with SNR on x axis and y-y_pred/sigma on y axis

    :param results:             dataframe with different predictions from a fixed set of parameters
                                results is produced by main.Visualising.utils
    :params folder_to_plot:     path to folder where to plot

    :return:   -
    """
    fig, axes=plt.subplots(nrows=2, ncols=2)
    plt.rcParams["figure.figsize"] = [15,15]
    list_of_seasons = list(set(results['season']))
    axes_ = axes.reshape(-1)
    min_x, max_x, min_y, max_y = get_min_max(results)


    for n in range(0, len(list_of_seasons)):
        temp = results[results['season']==list_of_seasons[n]]
        y1 = (temp['OC']-temp['OCf:Value'])/temp['OCf:Unc']
        x1 = temp['OCf:Value']/temp['OCf:Unc']

        axes_[n].scatter(x1, y1)
        #min_x, max_x, max_y = get_max_lims(axes)
        axes_[n].scatter(x1, y1)

        axes_[n].set_xlim([min_x, max_x])
        axes_[n].set_ylim([-max_y, max_y])
        axes_[n].axhline(0, color='red')
        axes_[n].axhline(-3, color='red', linestyle = '--')
        axes_[n].axhline(3, color='red', linestyle = '--')
        axes_[n].axvline(3, color='red')
        axes_[n].axvline(10, color='red')
        axes_[n].set_title(list_of_seasons[n], size =32)

        if n<2:
            axes_[n].set_xticks([])
        if (n)%2!=0:
            axes_[n].set_yticks([])

    fig.tight_layout()
    plt.savefig(os.path.join(folder_to_plot,'SNR_errors_per_season.pdf'))
    plt.rcParams["figure.figsize"] = [20,9]
    return
    

def plot_pdf_ratio_OC_per_cluster(results, folder_to_plot):
    """Plot estimated probability density function of OM/OC and OC (two subplots) for each cluster
    :param results:             dataframe with different predictions from a fixed set of parameters
                                results is produced by main.Visualising.utils
    :params folder_to_plot:     path to folder where to plot

    :return:   
    """
    from matplotlib import cm
    fig, axes=plt.subplots(nrows=2, ncols=1, figsize=(16,9))
    max_OC= np.percentile(results['OC'], 99)
    t_range_ratio = np.linspace(1, 4, 100)
    t_range_OC = np.linspace(0, max_OC, 100)
    color=iter(cm.tab10(np.linspace(0,1,len(list(set(results['cluster']))))))
    for cluster in list(set(results['cluster'])):
        c=next(color)
        results_temp = results[results['cluster']==cluster]
        kde_ratio = scipy.stats.gaussian_kde(results_temp['ratio'])
        axes[0].plot(t_range_ratio,kde_ratio(t_range_ratio),lw=2, label = cluster, c=c)
        kde_OC = scipy.stats.gaussian_kde(results_temp['OC'])
        axes[1].plot(t_range_OC,kde_OC(t_range_OC),lw=2, label = cluster, c=c)
    axes[0].set_xlabel('OM/OC')
    axes[1].set_xlabel('OC $\mu g/cm^2$')

    axes[0].legend(loc='best', title= 'Cluster')
    t2 = fig.text(0.07,0.55, r'Prob',rotation=90)
    fig.tight_layout()
    plt.savefig(os.path.join(folder_to_plot,'pdf_ratio_OC_per_cluser.pdf'),bbox_inches='tight', bbox_extra_artists=[t2] )

    plt.close()
    return


def generate_pdf_metropolis(run, template, clusters=None):
    """Produce and save tex and pdf file with all figures from files 1 and all figures 
    from files2. Files2 figures are with and without alpha

    :param run:        name of run
    :param template:    latex template
    :param clusters:    list of clusters

    :return:   -
    """
    folder_to_plot = get_folder_to_plot(run)

    files0 = ['metropolis']
    files1 = ['scatter', 'scatter_alpha', 'scatter_per_season', 'scatter_per_season_alpha', 'SNR_errors', 'SNR_errors_per_season','loglik_r']
    files2 = ['histo_ratios', 'per_season', 'ratio_per_site_and_season', 'ratio_per_type_and_season',
        'OC_FG','OM_FG', 'ratio_FG']
    files3 = ['scatter_per_cluster', 'pdf_ratio_OC_per_cluser']

    template.write_section('Metropolis')
    if clusters is not None:
        template.write('This analysis has been made with data from clusters '+', '.join(map(str, clusters)))

    for f in files1:
        tuple_ = (folder_to_plot, f+'.pdf')
        template.includegraphics(os.path.join(*tuple_), options=r'width=\textwidth')
    for f in files2:
        tuple_ = ( folder_to_plot, f+'.pdf')
        template.includegraphics(os.path.join(*tuple_), options=r'width=\textwidth')
    if clusters is not None:
        for f in files3:
            tuple_ = ( folder_to_plot, f+'.pdf')
            template.includegraphics(os.path.join(*tuple_), options=r'width=\textwidth')
    return template


def _check_length_params_plot_metropolis(params_to_be_plotted, bins):
    """Check that all params to plot metropolis have the same size and raise 
    error if not

    :params_to_be_plotted       params to be plotted for metropolis plot
    :bins                       bins for metropolis plot

    :return:   -
    """
    if (len(bins)!=len(params_to_be_plotted)):
        raise ValueError('Bins do not have the same size than list of params')
    return 


def generate_initial_param(run, model_spe, cluster = None):
    """Takes the best results from laplace to be used as initial vector of params for metropolis

        :param run:         name of run number
        :param model_spe:   model_spe dict     
        :param cluster:     cluster of interest

        :return             vector of inital parameter
        """
    model_spe = get_model_spe(run)
    path = path_laplace_LBFGS(run, cluster)
    path = os.path.join(path, 'results.csv')

    df = pd.read_csv(path)
    df = df[df['ncomp_aCOH']<18]
    MAP = df.sort_values('posterior', ascending = False).head(1)
    MAP = MAP[list(model_spe['order_params'].keys())]
    init = list(MAP.values[0])
    return(init)

def get_results(run, chain, bins, cluster = None, plot = None):
    """ Get metropolis results and plot posterior parameters distribution

    param run:          name of run
    param chain:        chain of interest
    param bins:         list of bins to be used to plot histogram of posterior distribution
    param cluster:      cluster of interest
    param plot:         if plot with histogram of posterior distribution must be plotted

    return:             MAP predictions and MAP

    """
    path_results = path_metropolis(run, cluster, number_of_chain = int(chain.replace('chain', '')))
    model_spe =  get_model_spe(run)
    params_to_be_plotted = list(model_spe['order_params'].keys()) + ['posterior']
    _check_length_params_plot_metropolis(params_to_be_plotted, bins)
    df1 = get_results_metropolis(run, chain, cluster)
    params1 = get_best_solution(df1, model_spe['order_params'])

    results1 = get_constituents(run, params1, cluster)
    results1['season']=results1['season'].apply(lambda x: get_season_string(x))
    results1['season'] = pd.Categorical(results1['season'], categories=['DJF','MAM','JJA','SON'])
    results1['O_C']= results1['O']/results1['C']

    if plot is not None:
        folder_to_plot = get_folder_to_plot(run)
        if cluster is not None:
            folder_to_plot = os.path.join(folder_to_plot, str(cluster).zfill(2))
            create_folder(folder_to_plot)
        if bins is not None:
            plot_distributions(params_to_be_plotted, bins, run, chain, model_spe, folder_to_plot, cluster)
    return results1, params1


def plot_metropolis(run, clusters = None, chain ='chain1', bins=_DEFAULT_BINS):
    """Produce and save all figure and tex and pdf file
    
    :param run:                 name of run
    :param clusters:            list of clusters
    :param chain:               chain from metropolis to be used
    :bins                       bins for metropolis plot

    :return:   -
    """

    if True:
        params =[]
        if clusters is not None:
            if type(clusters) is not list:
                clusters = [clusters]
            i = 0
            for cluster in clusters:
                if i == 0:
                    results1, params1 = get_results(run, chain, bins, cluster, plot = True)
                    results1['cluster'] = cluster
                    params.append(params1)

                else:
                    temp, params1 = get_results(run, chain, bins, cluster, plot = True)
                    temp['cluster'] = cluster
                    results1 = results1.append(temp)
                    params.append(params1)
                i  += 1
        else: 
            results1, params1 = get_results(run,  chain, bins, plot = True)

    folder_to_plot = get_folder_to_plot(run)
    plot_hist(results1, folder_to_plot)
    plot_ratios_per_season(results1, folder_to_plot)
    plot_scatter(results1, folder_to_plot)
    plot_scatter_per_season(results1, folder_to_plot)
    plot_scatter_per_type(results1, folder_to_plot)
    plot_scatter_alpha(results1, folder_to_plot)
    plot_scatter_per_type_alpha(results1, folder_to_plot)
    plot_OC_FG(results1, folder_to_plot)
    plot_OM_FG(results1, folder_to_plot)
    plot_ratio_FG(results1, folder_to_plot)

    plot_SNR_errors(results1, folder_to_plot)
    plot_SNR_errors_per_season(results1, folder_to_plot)
    plot_log_lik(results1, folder_to_plot)
    folder_to_plot = get_folder_to_plot(run)

    plot_scatter_per_season(results1, folder_to_plot)
    plot_scatter_per_season_alpha(results1, folder_to_plot)


    plot_ratio_per_site_and_season(results1, folder_to_plot)
    plot_ratio_per_type_and_season(results1, folder_to_plot)

    if clusters is not None:
        plot_scatter_per_cluster(results1, folder_to_plot)
        plot_pdf_ratio_OC_per_cluster(results1, folder_to_plot)
    return params

