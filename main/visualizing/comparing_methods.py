from pathlib import Path
import sys
import json
import os
import glob

import pandas as pd
import numpy as np 
import re
from scipy.integrate import trapz


path = Path().resolve()
sys.path.append(f'{path.parents[1]}/')


from main.models.paths_results import get_results_folder, path_metropolis, path_laplace_LBFGS, create_folder
from main.models.utils import read_json, get_model_spe
from main.visualizing.utils import titles_for_plots, plot_priors, plot_continuous, plot_prior_ncomps, plot_uniform
    
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
plt.rcParams["figure.figsize"] = [24,8]
plt.rcParams["axes.titlesize"] = 30
plt.rcParams["axes.labelsize"]= 26
plt.rcParams["xtick.labelsize"] = 20
plt.rcParams["ytick.labelsize"] = 20
plt.style.use('seaborn-deep')


# In[2]:


_DEFAULT_BINS = [50, 50, 50, 60, 60, 60]


# In[3]:



# In[4]:


def _handle_ticks(axes_, params_to_be_plotted):
    """ set limits for latent variables x axis
    params axes_                        matplotlib.pyplot axes flatten
    params params_to_be_plotted:        list of paremeters to be plotted

    return 
    """
    for n in range(0,len(params_to_be_plotted)):
        if params_to_be_plotted[n] in ['ncomp_aCH', 'ncomp_aCOH', 'ncomp_COOH']:
            axes_[n].set_xlim([0,20])

    return


# In[5]:


def _set_title(axes_, params_to_be_plotted):
    """ add title to distribution plot

    params axes_                        matplotlib.pyplot axes flatten
    params params_to_be_plotted:        list of paremeters to be plotted

    return 
    """
    for n in range(0,len(params_to_be_plotted)):
        axes_[n].set_xlabel(titles_for_plots[params_to_be_plotted[n]], size = 26)
    return


# In[6]:


def _get_number_of_chain(chain):
    """ get the number of chain from string

    param chain: string "chainx"

    return number: x
    """
    number = int(re.search(r'\d+', chain).group())
    return number


# In[7]:


def _read_run_config(path_chain):
    """Read configuration file for the run

    param path_chain: path to chain results

    return run_config: dict with run configuration
    """
    path_run = Path(path_chain).parents[0]
    run_config = read_json(os.path.join(path_run, 'run_config.json'))
    return(run_config)
    
    


# In[8]:


def _dataframe_results(path, config_run):
    """ Get metropolis results

    param path:          path to results
    param config_run:     dict specifying run configuration

    return:         metropolis chain

    """
    allFiles = sorted(Path(path).iterdir(), key=lambda f: f.stat().st_mtime)
    if not allFiles:
        raise ValueError('Not results for metropolis for run and chain specified:', path)
    list_ = []
    for n in range(1,len(allFiles)):
        file_ = allFiles[n]
        df = pd.read_csv(file_,index_col=None)
        list_.append(df)
    frame = pd.concat(list_)
    return frame
            


# In[9]:


def get_results_metropolis(path):
    """ Get metropolis results

    param path:          path to results
    param config_run:     dict specifying run configuration

    return:         metropolis chain

    """
    config_run = _read_run_config(path)
    df = _dataframe_results(path, config_run)
    return df


# In[10]:


def _set_title(axes, rows, params_to_be_plotted):
    """ Set title for each axes according to titles for plots

    param axes:         matplotlib axes
    param rows:         number of rows in plot
    params_to_be_plotted:       parameters to be plotted

    return: -
    """
    for n in range(0,len(params_to_be_plotted)):
        axes[rows-1, n].set_xlabel(titles_for_plots[params_to_be_plotted[n]], size = 26)
    return

def plot_laplace_LBFGS(axes, params_to_be_plotted, run, cluster, bins):
    """ Plot laplace marginal distribution on axes

    params axes:                matplotlib axes
    params_to_be_plotted:       parameters to be plotted
    params run:                 name of run
    param cluster:              cluster currently plotted
    param bins:                 list of bins to be used for each parameters

    return -

    """
    for n in range(0, len(params_to_be_plotted)):
        variable = params_to_be_plotted[n]
        path = path_laplace_LBFGS(run, cluster, variable)
        path = os.path.join(path, 'posterior.csv')
        df = pd.read_csv(path)
        df = df.dropna()
        df['marginal'] = df['marginal'] - np.max(df['marginal'])+5
        df['marginal'] = df['marginal'].apply(lambda x: np.exp(x))
        df['marginal'] = df['marginal']/ np.sum(df['marginal'])
        if variable in ['ncomp_aCH', 'ncomp_aCOH', 'ncomp_COOH']:
            axes[cluster-1, n].hist(df[variable].values+1.4, weights = df['marginal'], density= True, bins = np.linspace(0.4,30.4,31), range=(0.4,30.4), color='red', alpha =.5)
        else:
            area = trapz(df['marginal'], df[variable])
            area2 = trapz(df['marginal']/area, df[variable])
            axes[cluster-1, n].plot(df[variable],df['marginal']/area, color='red')
    
    return df

def plot_metropolis(axes, params_to_be_plotted, run, chain, cluster = None, color='blue', bins =[50, 50, 50, 50, 2, 4, 6]):
    """ Plot metropolis posterior distribution on axes

    params axes:                matplotlib axes
    params_to_be_plotted:       parameters to be plotted
    params run:                 name of run
    param chain:                chain of interest
    param cluster:              cluster currently plotted
    param color:                color of the plot
    param bins:                 list of bins to be used for each parameters

    return -

    """
    number_of_chain = _get_number_of_chain(chain)
    paths = path_metropolis(run = run, cluster = cluster, number_of_chain = number_of_chain)
    path = paths[chain]
    df = get_results_metropolis(path)
    
    df['ncomp_aCH'] = df['ncomp_aCH']+1
    df['ncomp_aCOH'] = df['ncomp_aCOH']+1
    df['ncomp_COOH'] = df['ncomp_COOH']+1
    
    
    for n in range(0,len(params_to_be_plotted)):
        range_ = None
        if params_to_be_plotted[n] in ['ncomp_aCH', 'ncomp_aCOH', 'ncomp_COOH']:
            range_ = (0, 30)
            axes[cluster-1, n].hist(df[params_to_be_plotted[n]], bins =  np.linspace(0,30,31), density = True, range =range_)
        else:
            axes[cluster-1,n].hist(df[params_to_be_plotted[n]],bins = bins[n], density = True, range =range_)
    
    return 

def plot_priors(run, params_to_be_plotted, axes, model_spe, cluster):
    """Central function to plot all priors on metropolis plot 
    :param run:                     run
    :param params_to_be_plotted:    all the parameters to be plotted
    :param axes:                    axes of metropolis plot

    :return: -
    """
    model_spe = get_model_spe(run)
    for n in range(0, len(params_to_be_plotted)):
        variable = params_to_be_plotted[n]
        if('prior_'+variable in list(model_spe.keys())):
            if variable in ['ncomp_aCH','ncomp_aCOH','ncomp_COOH']:
                plot_prior_ncomps(variable, model_spe, axes[cluster-1, n])
            else:
                pdf = plot_continuous(variable, model_spe, axes[cluster-1, n])
        elif variable !='posterior':
            plot_uniform(variable, model_spe, axes_[n])
            
    return
    

def generate_pdf_comparing(run,  template, cluster= None):
    """ add pdf comparing results from laplace and metropolis in report.pdf file

    param run:          name of run
    param template:     latex template
    param cluster:      cluster of interest 
    """
    folder_to_plot = os.path.join(get_results_folder(run), 'visualising')

    template.write_section('Comparing methods results')
    tuple_ =(folder_to_plot, 'comparison_methods.pdf')
    template.includegraphics(os.path.join(*tuple_), options=r'width=\textwidth')

    return template




def plot_comparing_all(run, clusters = None, chain = 'chain1', bins = _DEFAULT_BINS):
    """Plot comparison laplace and metropolis estimation
    param run:          name of run
    param clusters:     list of clusters
    param chain:        chain of interest
    param bins:         list of bins for each parameters histogram

    return 
    """
    folder_to_plot = os.path.join(get_results_folder(run), 'visualising')
    model_spe = get_model_spe(run)
    params_to_be_plotted = list(model_spe['order_params'].keys())
    col = 6
    params_to_be_plotted = list(model_spe['order_params'].keys())
    if 'kappa' in params_to_be_plotted:
        col = 7
    
    fig, axes = plt.subplots(nrows=len(clusters), ncols=col, figsize=(24,2*col), sharex='col')

    if len(clusters)==1:
        axes = axes.reshape(1, col)


    _set_title(axes, len(clusters), params_to_be_plotted)
    params = []
    patches = []
    patches.append(mpatches.Patch(color='blue', label='Metropolis'))
    patches.append(mpatches.Patch(color='red', label='Laplace'))
    patches.append(mpatches.Patch(color='black', label='Prior'))

    for cluster in range(1,len(clusters)+1):
        if os.listdir(path_metropolis(run, cluster)['chain1']):
            plot_metropolis(axes, params_to_be_plotted, run, 'chain1', cluster, color='blue', bins = bins)
        if os.listdir(path_laplace_LBFGS(run, cluster)):
            plot_laplace_LBFGS(axes, params_to_be_plotted, run, cluster, bins)
        plot_priors(run, params_to_be_plotted, axes, model_spe, cluster)
        axes[cluster-1, 0].set_ylabel('Cluster '+str(cluster), size = 22)
    axes[0,0].set_xlim([.5,.9])
    axes[0,1].set_xlim([.35,.6])
    axes[0,2].set_xlim([0,1])
    axes[0,3].set_xlim([0,0.15])
    axes[0,4].set_xlim([0,20])
    axes[0,5].set_xlim([0,20])
    axes[0,6].set_xlim([0,20])
    for n in range(0, col):
        for j in range(0, len(clusters)):
            axes[j,n].set_yticks([])

    t2 = fig.text(0.08, 0.5, 'Density', va='center', rotation='vertical', size = 28)
    plt.subplots_adjust(wspace=.1, hspace=0.1)

    legends=axes[0, col-1].legend(handles=patches,  bbox_to_anchor=(1.5, 1.02), prop={'size': 18})

    plt.savefig(os.path.join(folder_to_plot, 'comparison_methods.png'),format='png',bbox_inches='tight', bbox_extra_artists=[legends, t2] )

    return 
